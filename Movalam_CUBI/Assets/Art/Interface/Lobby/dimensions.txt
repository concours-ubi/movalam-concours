## Bandeaux des titres

Le bandeau fait 70px de haut et est à 150px du haut de l'écran
Chaque texte est à 70px de la gauche de son cadre

### LOBBY LIST

## Lobby List

Chaque rangée fait 125px de haut, pas d'écart entre chaque
Cadre de preview fait 160x100, à 80px de la gauche
Nom du lobby à 280px de la gauche
Nom du niveau à 630px de la gauche
Nombre de joueurs à 840px de la gauche
Le fond jaune est présent seulement lorsque le lobby est selectionné
Le cadre complet fait 960x840

## Actions

Chaque bouton fait 50px de haut
Le texte est à 100px de la gauche

### LOBBY CREATION

Le cadre du nom de lobby fait 400x50, à 80px de la gauche et 130px du haut du cadre
Les boutons font 195x40, à 80px de leurs bords respectifs
Les boutons en bas font 50px de haut, à 60px du abs du cadre pour le plus bas