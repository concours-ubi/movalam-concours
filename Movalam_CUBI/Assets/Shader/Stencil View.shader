Shader "Custom/Stencil View"
{
    SubShader
    {
        Tags {"Queue"= "Geometry-100"}
        ColorMask 0
        Zwrite off
        
        Pass{
            Stencil {
                Ref 1
                Pass Replace
            }
        }
    }
}
