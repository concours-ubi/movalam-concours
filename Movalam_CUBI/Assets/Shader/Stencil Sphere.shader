Shader "Custom/Stencil Sphere"
{
    SubShader
    {
    Tags{ "Queue" = "Geometry-90" }
        ColorMask 0
        Zwrite off

        Pass{
            Stencil {
                Ref 2
                Pass Replace
            }
        }
    }
}
