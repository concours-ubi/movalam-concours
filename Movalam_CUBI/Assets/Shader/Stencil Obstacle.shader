Shader "Custom/Stencil Obstacle"
{
    SubShader
    {
        Tags {"Queue" = "Geometry-50"}
        ColorMask 0
        Zwrite off

        Pass{
            Stencil {
                Ref 2
                Pass Replace
            }
        }
    }
}
