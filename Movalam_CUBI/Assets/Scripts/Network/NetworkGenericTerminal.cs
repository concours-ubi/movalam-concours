using UnityEngine;
using Mirror;
using System.Collections.Generic;

/*
	Documentation: https://mirror-networking.com/docs/Guides/NetworkBehaviour.html
	API Reference: https://mirror-networking.com/docs/api/Mirror.NetworkBehaviour.html
*/

public class NetworkGenericTerminal : NetworkBehaviour
{
    public GenericTerminal localTerminal;

    [SyncVar]
    public NetworkIdentity interactingWith;

    [SyncVar(hook = "SetLocalTerminalCryptedState")]
    public bool cryptedNetwork;

    [SyncVar(hook = "SetLocalTerminalInteractingState")]
    public bool interactingStateNetwork;

    protected virtual void Awake()
    {
        localTerminal = GetComponentInChildren<GenericTerminal>();
        localTerminal.doUpdate = false;
        localTerminal.OnDecryptEvent += Decrypt;
        localTerminal.OnEndInteractionEvent += () => {
            ReleaseClientauthority();
        };
        if (isServer)
            cryptedNetwork = localTerminal.crypted;
    }

    public virtual void Update()
    {
        //TODO if agent is down while interacting trigger EndInteraction
        if (localTerminal.triggerActive)
        {
            if (!hasAuthority)
            {
                if (!interactingStateNetwork && !NetworkAgent.localAgent.detected && !NetworkAgent.localAgent.movementController.GetPaused())
                {
                    if (Input.GetKeyDown(KeyCode.E))
                        NetworkAgent.localAgent.CmdGiveTerminalAuthority(this);
                }
            }
            else
            {
                if ((Input.GetKeyDown(KeyCode.E) || NetworkAgent.localAgent.detected) && localTerminal.isInteracting)
                {
                    localTerminal.EndInteraction();
                }
            }
        }
    }

    #region Interaction Message
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.transform.parent)
            {
                NetworkAgent networkAgent = other.transform.parent.GetComponentInParent<NetworkAgent>();
                if (networkAgent)
                {
                    if (networkAgent.isLocalPlayer)
                    {
                        localTerminal.interactionAgentPanel = other.gameObject.GetComponent<InteractionController>().interactionPanel;
                        localTerminal.interactionMessage = other.gameObject.GetComponent<InteractionController>().interactionText;
                        localTerminal.ShowInteractionMessage();
                        localTerminal.playerMouvement = networkAgent.movementController;
                        localTerminal.triggerActive = true;
                    }
                }
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.transform.parent)
            {
                NetworkAgent networkAgent = other.transform.parent.GetComponentInParent<NetworkAgent>();
                if (networkAgent)
                {
                    if (networkAgent.isLocalPlayer)
                    {
                        localTerminal.HideInteractionMessage();
                        localTerminal.interactionAgentPanel = null;
                        localTerminal.interactionMessage = null;
                        localTerminal.triggerActive = false;
                    }
                }
            }
        }
    }
    #endregion

    #region Decrypt
    protected void Decrypt()
    {
        if (isServer)
            cryptedNetwork = false;
        else
            CmdDecrypt();
    }

    [Command]
    protected void CmdDecrypt()
    {
        cryptedNetwork = false;
    }
    public void SetLocalTerminalCryptedState(bool oldState, bool newState)
    {
        localTerminal.crypted = newState;
    }
    #endregion

    #region Interacting State
    public void SetInteractingState(bool state)
    {
        if (isServer)
        {
            interactingStateNetwork = state;
        }
        else
        {
            CmdSetInteractingState(state);
        }
    }

    [Command]
    protected void CmdSetInteractingState(bool state)
    {
        interactingStateNetwork = state;
    }

    [Command]
    protected void CmdSetInteractingWith(NetworkIdentity networkIdentity)
    {
        interactingWith = networkIdentity;
    }

    public void SetLocalTerminalInteractingState(bool oldState, bool newState)
    {
        localTerminal.isInteracting = newState;
    }
    #endregion

    #region Network Authority
    public override void OnStartAuthority()
    {
        localTerminal.playerMouvement = NetworkAgent.localAgent.movementController;
        localTerminal.Interaction();
    }

    public override void OnStopAuthority()
    {
    }

    [Command]
    public void ReleaseClientauthority()
    {
        interactingStateNetwork = false;
        interactingWith.GetComponent<NetworkAgent>().interacting = false;
        interactingWith = null;
        netIdentity.RemoveClientAuthority();
    }
    #endregion
}
