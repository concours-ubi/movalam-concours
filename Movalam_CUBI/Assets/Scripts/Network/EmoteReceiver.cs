using UnityEngine;
using Mirror;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
public struct EmoteID : NetworkMessage
{
    public int id;
}

public class EmoteReceiver : NetworkBehaviour
{
    [SerializeField] private Emote[] emoteList;                 // Liste des emotes
    [SerializeField] private Image emoteImage;                  // R�f�rence sur l'image pour afficher l'emote
    [SerializeField] private Canvas uxAgent;                    // R�f�rence sur le canvas de l'ux agent

    private Queue emoteQueue = new Queue();                     // Queue pour les emotes qui arrivent
    private bool displaying = false;                            // Est-ce qu'une emote est actuellement affich�e
    [SerializeField] private float lastEmoteDisplayTime = 3;    // Temps d'affichage de la derni�re emote de la Queue
    [SerializeField] private float emoteDisplayTime = 1;        // Temps d'affichage de l'emote
    private float displayingTime = 0;                           // Temps que l'emote actuelle est affich�e

    private Color transparent = new Color(1, 1, 1, 0);          // Couleur transparente quand on re�oit pas d'emote
    private Color visible = new Color(1, 1, 1, 1);              // Couleur normale quand on re�oit une emote
    public AK.Wwise.Event wwiseEventReceiveEmote;

    void Start()
    {
        if (isLocalPlayer)
        {
            //emoteImage.enabled = true;
            uxAgent.enabled = true;
            NetworkClient.RegisterHandler<EmoteID>(OnEmoteID);
        }
    }

    private void Update()
    {
        if (displaying)
            displayingTime += Time.deltaTime;

        if (emoteQueue.Count > 0)
        {
            if (!displaying || (displaying && displayingTime > emoteDisplayTime))
            {
                emoteImage.color = visible;
                wwiseEventReceiveEmote.Post(gameObject);
                emoteImage.sprite = ((Emote)emoteQueue.Dequeue()).sprite;
                displayingTime = 0;
                displaying = true;
            }
        }
        else if (displaying && displayingTime > lastEmoteDisplayTime)
        {
            emoteImage.color = transparent;
            emoteImage.sprite = null;
            displaying = false;
        }
    }

    private void OnEmoteID(NetworkConnection conn, EmoteID emoteID)
    {
        emoteQueue.Enqueue(emoteList.First(emote => emote.correspondingNumber == emoteID.id));
    }
}
