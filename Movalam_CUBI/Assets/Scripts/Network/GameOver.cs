using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

struct GameOverMessage : NetworkMessage
{
}

public class GameOver : NetworkBehaviour
{
    public void SendGameOver()
    {
        if (isServer)
        {
            NetworkServer.SendToAll<GameOverMessage>(new GameOverMessage { });
        }
        else
        {
            CmdSendGameOver();
        }
    }

    [Command]
    public void CmdSendGameOver()
    {
        NetworkServer.SendToAll<GameOverMessage>(new GameOverMessage { });
    }
}
