using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkDetectorTerminal : NetworkGenericTerminal
{
    private DetectorTerminal detectorTerminal;

    private void Start()
    {
        base.Awake();
        detectorTerminal = GetComponentInChildren<DetectorTerminal>();
        detectorTerminal.OnDesactivateDetectorEvent += DesactivateDetector;
    }
    public void DesactivateDetector()
    {
        if (isServer)
        {
            detectorTerminal.detectorToDeactivate.SetActive(false);
            ClientRpcDesactivateDetector();
        }
        else
        {
            CmdDesactivateDetector();
        }
    }
    [Command]
    public void CmdDesactivateDetector()
    {
        detectorTerminal.detectorToDeactivate.SetActive(false);
        ClientRpcDesactivateDetector();
    }
    [ClientRpc]
    public void ClientRpcDesactivateDetector()
    {
        detectorTerminal.detectorToDeactivate.SetActive(false);
    }
}
