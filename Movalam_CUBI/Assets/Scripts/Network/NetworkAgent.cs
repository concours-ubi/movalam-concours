using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Mirror;
using Steamworks;

public class NetworkAgent : NetworkBehaviour
{
    public static NetworkAgent localAgent = null;
    public MovementController movementController;
    public DetectionController detectionController;
    public PauseController pauseController;
    public InteractionController interactionController;
    public AgentPositionController banner;
    public UXAgentController agentController;
    public TextMeshProUGUI displayName;
    private EmoteReceiver emoteReceiver;
    public Camera agentCamera;
    public float deadAtTime;
    public float maxDetectionTime;

    public delegate void DetectionChangedEvent(float detection);
    public DetectionChangedEvent OnDetectionChangedEvent;

    #region network properties

    [SyncVar(hook = nameof(SetAgentName))]
    public string playerName;

    [SyncVar]
    public ulong steamId;

    [SyncVar(hook = nameof(SetDetectedState))]
    public bool detected;

    [SyncVar(hook = nameof(SetInteractingState))]
    public bool interacting;

    [SyncVar]
    public bool dead;

    [SyncVar(hook = nameof(SetDetection))]
    public float detection;

    public SyncList<NetworkIdentityLink> networkIdentityLinks = new SyncList<NetworkIdentityLink>();
    private List<PlayerBannerController> netIdPlayerBannerMap = new List<PlayerBannerController>();
    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        networkIdentityLinks.Callback += AddBannerController;
        RemoveAllBanner();
    }

    void Start()
    {
        int slot;
        movementController.enabled = false;
        if (!isLocalPlayer)
        {
            agentController.enabled = false;
            agentController.gameObject.SetActive(false);
            agentCamera.enabled = false;
            //agentCamera.gameObject.SetActive(false);
            detectionController.detectionBar.enabled = false;
            detectionController.enabled = false;
            pauseController.enabled = false;
            interactionController.enabled = false;
            displayName.gameObject.SetActive(true);
            GetComponentInChildren<AkAudioListener>().enabled = false;
            slot = LobbyManager.Instance.GetSlotIndex(LobbyManager.Instance.GetMemberFromSteamId(steamId));
        }
        else
        {
            MurTransparent.player = movementController.gameObject;
            localAgent = this;
            GetComponent<EmoteReceiver>().enabled = true;
            GetComponent<EndgameReceiver>().enabled = true;
            GetComponent<GameOverReceiver>().enabled = true;
            detectionController.OnDetectedEvent += () => {
                detected = true;
                SendPlayerDownMessage();
            };
            slot = LobbyManager.Instance.GetSlotIndex(LobbyManager.Instance.GetMemberFromSteamId(SteamClient.SteamId));
            SynchroStart.OnGameStarted += EnableController;
        }
        banner.SetAgentBySlot(slot);
        SetAgentCharacter(slot - LobbyManager.Instance.currentLevel.AssitantCount);
    }
    private void OnDestroy()
    {
        SynchroStart.OnGameStarted -= EnableController;
    }

    private void Update()
    {
        if (isServer)
        {
            if (detected && !dead)
            {
                if ((float)NetworkTime.time >= deadAtTime)
                {
                    AgentDead();
                }
            }
        }

        if (isLocalPlayer)
        {
            if (detection != detectionController.detectionValue)
            {
                if (isServer)
                    detection = detectionController.detectionValue;
                else
                    CmdSendDetection(detectionController.detectionValue);
            }
        }
    }
    #endregion

    #region SyncVar Hooks

    public void SetDetection(float old, float detection)
    {
        OnDetectionChangedEvent?.Invoke(detection);
    }

    public void SetAgentName(string oldName, string newName)
    {
        displayName.text = newName;
    }

    public void SetDetectedState(bool oldState, bool newState)
    {
        if (newState)
        {
            //detected
            movementController.Detected();
        }
    }

    public void SetInteractingState(bool oldState, bool newState)
    {
        if (newState)
        {
            movementController.Interacts();
        }
        else
        {
            movementController.StopInteracting();
        }
    }

    public GameObject InitPlayerBanner(NetworkIdentityLink networkIdentityLink)
    {
        SteamProfile agentSteamProfile = LobbyManager.Instance.profiles[networkIdentityLink.steamId];
        PlayerBannerController agentBanner = agentController.AddPlayerBannerController(agentSteamProfile.steamName, agentSteamProfile.steamAvatar);
        agentBanner.SetNetworkdIdentity(networkIdentityLink.networkIdentity);
        netIdPlayerBannerMap.Add(agentBanner);
        return agentBanner.gameObject;
    }

    public void AddBannerController(SyncList<NetworkIdentityLink>.Operation op, int itemIndex, NetworkIdentityLink networkIdentityLinkOld, NetworkIdentityLink networkIdentityLinkNew)
    {
        switch (op)
        {
            case SyncList<NetworkIdentityLink>.Operation.OP_ADD:
                GameObject banner = InitPlayerBanner(networkIdentityLinkNew);
                if (itemIndex == 0)
                {
                    RectTransform rect = banner.GetComponent<RectTransform>();
                    rect.sizeDelta *= new Vector2(1.25f, 1);
                }
                break;
            case SyncList<NetworkIdentityLink>.Operation.OP_CLEAR:
                RemoveAllBanner();
                break;
            case SyncList<NetworkIdentityLink>.Operation.OP_INSERT:
                InitPlayerBanner(networkIdentityLinkNew).transform.SetSiblingIndex(itemIndex);
                break;
            case SyncList<NetworkIdentityLink>.Operation.OP_REMOVEAT:
                PlayerBannerController agentBanner = netIdPlayerBannerMap.Find(banner => banner.GetNetworkIdentity().netId == networkIdentityLinkOld.networkIdentity.netId);
                GameObject playerBannerRoot = agentBanner.gameObject;
                netIdPlayerBannerMap.Remove(agentBanner);
                Destroy(playerBannerRoot);
                break;
            case SyncList<NetworkIdentityLink>.Operation.OP_SET:
                Debug.LogError("Set in AddBannerController");
                break;
            default:
                break;
        }
    }

    public void RemoveAllBanner()
    {
        foreach (Transform child in agentController.playerPanel)
        {
            Destroy(child.gameObject);
        }
    }

    public PlayerBannerController GetPlayerBannerController(uint netId)
    {
        for (int i = 0; i < netIdPlayerBannerMap.Count; i++)
        {
            if (netId == netIdPlayerBannerMap[i].GetNetworkIdentity().netId)
                return netIdPlayerBannerMap[i];
        }
        return null;
    }

    #endregion

    #region Server command &  function

    [Command]
    private void CmdSendDetection(float detection)
    {
        this.detection = detection;
    }

    [ClientRpc]
    private void ClientRpcSetDetection(float detection)
    {
        OnDetectionChangedEvent?.Invoke(detection);
    }

    /// <summary>
    ///     Message de mise au sol d'un agent envoyer a tout le monde
    /// </summary>
    [Command]
    private void SendPlayerDownMessage()
    {
        detected = true;
        deadAtTime = (float)NetworkTime.time + LevelRuleSet.Instance.levelRuleSet.timeBeforeAgentDead;
        NetworkServer.SendToAll<AgentDownMessage>(
            new AgentDownMessage
            {
                senderId = netIdentity.netId,
                detectedTime = (float)NetworkTime.time,
                detectionTimeEnd = deadAtTime
            });
    }

    [Command]
    public void CmdGiveTerminalAuthority(NetworkGenericTerminal networkGenericTerminal)
    {
        //TODO vérifier que la borne n'a pas d'authorité
        networkGenericTerminal.netIdentity.AssignClientAuthority(connectionToClient);
        networkGenericTerminal.interactingStateNetwork = true;
        interacting = true;
        networkGenericTerminal.interactingWith = netIdentity;
        //TODO replicater l'info que l'agent interagi
    }

    public void AgentDead()
    {
        dead = true;
        //LevelRuleSet.Instance.levelRuleSet.detectionTime -= LevelRuleSet.Instance.gameRuleSet.detectionTime * LevelRuleSet.Instance.levelRuleSet.detectionTimeReductionFactor;
    }
    #endregion

    [ClientRpc]
    public void RespawnAgentOnNetwork()
    {
        detected = false;
        detectionController.Respawn();
    }

    [ContextMenu("Respawn")]
    public void ContextMenuRespawn()
    {
        detected = false;
        detectionController.Respawn();
    }

    public void SetAgentCharacter(int slot)
    {
        movementController.SetAgentCharacter(slot);
        NetworkAnimator animNet = GetComponent<NetworkAnimator>();
        animNet.animator = movementController.animator;
    }

    public void EnableController()
    {
        movementController.enabled = true;
    }
}
