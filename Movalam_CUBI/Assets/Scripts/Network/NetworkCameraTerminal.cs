using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkCameraTerminal : NetworkGenericTerminal
{
    private CameraTerminal cameraTerminal;

    private void Start()
    {
        base.Awake();
        cameraTerminal = GetComponentInChildren<CameraTerminal>();
        cameraTerminal.OnDesactivateCameraEvent += DesactivateCamera;
    }

    public void DesactivateCamera()
    {
        if (isServer)
        {
            cameraTerminal.cameraToDeactivate.SetActive(false);
            ClientRpcDesactivateCamera();
        }
        else
        {
            CmdDesactivateCamera();
        }
    }

    [Command]
    public void CmdDesactivateCamera()
    {
        cameraTerminal.cameraToDeactivate.SetActive(false);
        ClientRpcDesactivateCamera();
    }

    [ClientRpc]
    public void ClientRpcDesactivateCamera()
    {
        cameraTerminal.cameraToDeactivate.SetActive(false);
    }
}
