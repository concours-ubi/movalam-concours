using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeJoignability : MonoBehaviour
{
    public int joignability = 0;
    private TextMeshProUGUI guiText;
    private string[] textList = { "PUBLIC", "FRIENDS ONLY", "PRIVATE" };

    private void Start()
    {
        guiText = GetComponent<TextMeshProUGUI>();
    }

    public void ChangeLeft()
    {
        if (joignability != 0)
            joignability = (joignability - 1) % 3;
        else if (joignability == 0)
            joignability = 2;
        ChangeText();
    }

    public void ChangeRight()
    {
        joignability = (joignability + 1) % 3;
        ChangeText();
    }

    public void ChangeText()
    {
        try
        {
            if (joignability == 0)
                guiText.text = textList[0];
            if (joignability == 1)
                guiText.text = textList[1];
            if (joignability == 2)
                guiText.text = textList[2];
        }
        catch { }
    }
}
