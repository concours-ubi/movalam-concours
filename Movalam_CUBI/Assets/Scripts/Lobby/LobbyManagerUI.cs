using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Steamworks;
using Steamworks.Data;
using Mirror;
using System;
using System.Threading.Tasks;
using Mirror;

public class LobbyManagerUI : MonoBehaviour
{
    public LobbyProfile lobbyProfile;
    public Transform mainmenu;
    public Transform tutoriel;
    public Transform settings;
    public Transform credit;
    public Transform lobbyCreationInterface;
    public Transform lobbyInterface;
    public Transform lobbiesList;

    public GameObject playerSlotPrefab;
    public SlotUI[] slotsUI;
    public Transform assistantList;
    public Transform agentList;
    public Button readyStartButton;
    public TextMeshProUGUI readyStartButtonLabel;
    private UnityEngine.Color baseColor;
    private UnityEngine.Color invalidcolor = UnityEngine.Color.red;

    public Transform failedConnectionAttemptUI;

    private void Awake()
    {
        LobbyManager.OnLobbyUpdateEvent += LobbyUpdateUI;
        LobbyManager.OnLobbyEnteredEvent += OnLobbyEntered;
        LobbyManager.OnLobbyLeaveEvent += OnLobbyLeave;
        SteamMatchmaking.OnLobbyMemberLeave += OnLobbyMemberLeave;
        CustomNetworkManager.OnClientErrorEvent += OnClientError;
        baseColor = readyStartButtonLabel.color;
        if (LobbyManager.Instance.currentLobby.Id != 0)
        {
            OnLobbyEntered(LobbyManager.Instance.currentLobby);
        }
    }

    private void OnDestroy()
    {
        LobbyManager.OnLobbyUpdateEvent -= LobbyUpdateUI;
        LobbyManager.OnLobbyEnteredEvent -= OnLobbyEntered;
        LobbyManager.OnLobbyLeaveEvent -= OnLobbyLeave;
    }
    private void LobbyUpdateUI(Lobby lobby)
    {
        for (int i = 0; i < LobbyManager.Instance.slots.Length; i++)
        {
            if (!LobbyManager.Instance.slots[i].isAvailable)
                slotsUI[i].LoadPlayer(LobbyManager.Instance.slots[i]);
            else
                slotsUI[i].UnloadPlayer();

        }
        SetStartButton();
        if (LobbyManager.Instance.currentLobby.Owner.Id == SteamClient.SteamId)
        {
            readyStartButton.interactable = LobbyManager.Instance.isLobbyReady;
            readyStartButtonLabel.color = LobbyManager.Instance.isLobbyReady ? baseColor : invalidcolor;
        }
    }

    public void LoadLobby()
    {
        lobbyProfile.LoadLobby(LobbyManager.Instance.currentLobby);
        slotsUI = new SlotUI[LobbyManager.Instance.currentLevel.TotalCount];
        foreach (Transform row in assistantList.transform)
        {
            if (row.GetComponent<SlotUI>())
                GameObject.Destroy(row.gameObject);
        }

        foreach (Transform row in agentList.transform)
        {
            if (row.GetComponent<SlotUI>())
                GameObject.Destroy(row.gameObject);
        }
        for (int i = 0; i < LobbyManager.Instance.currentLevel.AssitantCount; i++)
        {
            SlotUI slot = Instantiate(playerSlotPrefab, assistantList).GetComponent<SlotUI>();
            slot.slotIndex = i;
            slotsUI[i] = (slot);
        }
        for (int i = 0; i < LobbyManager.Instance.currentLevel.AgentCount; i++)
        {
            SlotUI slot = Instantiate(playerSlotPrefab, agentList).GetComponent<SlotUI>();
            slot.slotIndex = LobbyManager.Instance.currentLevel.AssitantCount + i;
            slotsUI[LobbyManager.Instance.currentLevel.AssitantCount + i] = slot;
        }
        SetStartButton();
    }

    private void OnLobbyMemberLeave(Lobby lobby, Friend member) {
        SetStartButton();
    }
    private void SetStartButton()
    {
        readyStartButton.interactable = true;
        readyStartButtonLabel.color = baseColor;
        if (LobbyManager.Instance.currentLobby.Owner.Id == SteamClient.SteamId)
        {
            readyStartButtonLabel.text = "Start";
            readyStartButton.onClick.RemoveAllListeners();
            readyStartButton.onClick.AddListener(delegate
            {
                if (LobbyManager.Instance.isLobbyReady)
                {
                    LoadingScreen.ShowLoadingScreen(() => { LobbyManager.Instance.StartHostGame(); });
                }
            });
        }
        else
        {
            bool gameStartState = bool.Parse(LobbyManager.Instance.currentLobby.GetData("startGame"));
            if (gameStartState)
            {
                readyStartButtonLabel.text = "Join";
                readyStartButton.onClick.RemoveAllListeners();
                readyStartButton.onClick.AddListener(delegate
                {
                    LoadingScreen.ShowLoadingScreen(() => { LobbyManager.Instance.JoinGame(); });
                });
            }
            else
            {
                readyStartButtonLabel.text = "Ready";
                readyStartButton.onClick.RemoveAllListeners();
                readyStartButton.onClick.AddListener(delegate
                {
                    string readyStateData = LobbyManager.Instance.currentLobby.GetMemberData(new Friend(SteamClient.SteamId), "readyState");
                    if (readyStateData == "False" || readyStateData == "True")
                    {
                        LobbyManager.Instance.currentLobby.SetMemberData("readyState", readyStateData == "True" ? "False" : "True");
                    }
                });
            }
        }
    }

    private void OnLobbyEntered(Lobby lobby)
    {
        // Switch ui
        mainmenu.gameObject.SetActive(false);
        tutoriel.gameObject.SetActive(false);
        settings.gameObject.SetActive(false);
        credit.gameObject.SetActive(false);
        lobbyCreationInterface.gameObject.SetActive(false);
        lobbiesList.gameObject.SetActive(false);
        lobbyInterface.gameObject.SetActive(true);
        LoadLobby();
        LobbyUpdateUI(lobby);
    }

    private void OnLobbyLeave()
    {
        // Switch ui
        lobbyCreationInterface.gameObject.SetActive(false);
        lobbyInterface.gameObject.SetActive(false);
        lobbiesList.gameObject.SetActive(true);
    }

    public void LeaveLobbyTrigger()
    {
        LobbyManager.Instance.LeaveLobby();
    }

    public void OnClientError()
    {
        if (LobbyManager.Instance.connectionAttemp < 2)
        {
            LoadingScreen.HideLoadingScreen(() => { });
            failedConnectionAttemptUI.gameObject.SetActive(true);
        }
    }

    public void TriggerReconnectAttempt()
    {
        LobbyManager.Instance.ReconnectAttempt();
    }
}
