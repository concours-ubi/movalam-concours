using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public struct LobbyParameters
{
    public enum privacyLevel
    {
        Public,
        FriendsOnly,
        Private
    }

    public string lobbyName;
    public LevelAsset levelAsset;
    public privacyLevel joignabilityLevel;
    public bool visibilityLevel; // 0 = invisible / 1 = visible

    public LobbyParameters(string lobbyName, privacyLevel joignabilityLevel, bool visibilityLevel, LevelAsset levelAsset) : this()
    {
        this.lobbyName = lobbyName;
        this.joignabilityLevel = joignabilityLevel;
        this.visibilityLevel = visibilityLevel;
        this.levelAsset = levelAsset;
    }
}
public class LobbyCreationUI : MonoBehaviour
{
    public TMP_InputField inputName;
    public ChangeJoignability joignabilityScript;
    public ChangeVisibility visibilityScript;
    public LevelAssetUI levelAssetUI;
    public Button creationButton;
    public TextMeshProUGUI creationButtonLabel;
    public Color baseColor;
    private Color invalidColor = Color.red;

    private void Awake()
    {
        creationButtonLabel = creationButton.GetComponentInChildren<TextMeshProUGUI>();
        baseColor = creationButtonLabel.color;

    }

    public void OnEnable()
    {
        inputName.text = "";
        joignabilityScript.joignability = 0;
        joignabilityScript.ChangeText();
        visibilityScript.invisible = false;
        visibilityScript.ChangeText();
        levelAssetUI = null;
        OnInputLobbyNameChange();
    }

    public void OnInputLobbyNameChange()
    {
        if(levelAssetUI)
        {
            creationButton.interactable = inputName.text.Length > 3;
            creationButtonLabel.color = creationButton.interactable ? baseColor : invalidColor;
        }
        else
        {
            creationButton.interactable = false;
            creationButtonLabel.color = invalidColor;
        }
    }

    public void OnLevelSelected()
    {
        if(inputName.text.Length > 3)
        {
            creationButton.interactable = true;
            creationButtonLabel.color = baseColor;
        }
        else
        {
            creationButton.interactable = false;
            creationButtonLabel.color = invalidColor;
        }
    }

    public void CreateLobbyTrigger()
    {
        if(levelAssetUI)
        {
            LobbyParameters lobbyParameters = new LobbyParameters(
            inputName.text,
            (LobbyParameters.privacyLevel)joignabilityScript.joignability,
            visibilityScript.invisible,
            levelAssetUI.levelAsset);
            _ = SteamManager.CreateLobby(lobbyParameters);
        }
    }
}
