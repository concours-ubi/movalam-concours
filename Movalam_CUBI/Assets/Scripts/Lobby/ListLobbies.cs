using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;
using Steamworks.Data;
using System.Threading.Tasks;
using System;

public class ListLobbies : MonoBehaviour
{
    Lobby[] lobbies;
    Dictionary<string, string> customFilters;
    public bool showAvailableLobbyOnly { get; set; }

    public GameObject lobbyRowPrefab;
    public Transform listObject;

    public delegate void lobbySelected(Lobby lobby);
    public static lobbySelected OnLobbySelected;

    public Button refreshButton;

    public GameObject quickPlayInterface;

    private void Awake()
    {
        customFilters = new Dictionary<string, string>();
    }
    private void OnEnable()
    {
        TriggerRefreshLobbies();
    }

    public async void TriggerRefreshLobbies()
    {
        if (refreshButton)
            refreshButton.interactable = false;

        foreach (Transform row in listObject.transform)
        {
            GameObject.Destroy(row.gameObject);
        }
        lobbies = await GetLobbyList();
        if (refreshButton)
            refreshButton.interactable = true;
        if (lobbies != null)
            LoadLobbies();
    }

    public static async Task<Lobby[]> GetLobbyList()
    {
        try
        {
            //Query
            LobbyQuery lobbyQuery = SteamMatchmaking.LobbyList;
            if (SteamManager.Instance.appid == 480)
            {
                lobbyQuery = lobbyQuery.WithKeyValue("game", "movaclam");
            }

            //Async Request
            lobbyQuery = lobbyQuery.FilterDistanceWorldwide();
            Lobby[] lobbiesArray = await lobbyQuery.RequestAsync();
            if (lobbiesArray != null)
            {
                //Data manipulation

                //ping ?
                //foreach (Lobby lobby in lobbies)
                //{
                //    bool gameStarted = bool.Parse(lobby.GetData("gameStart"));
                //    if (gameStarted)
                //    {
                //        uint ip = 0;
                //        ushort port = 0;
                //        SteamId steamId = new SteamId();
                //        if (lobby.GetGameServer(ref ip, ref port, ref steamId))
                //        {
                //            ServerInfo lobbyServerInfo = new ServerInfo(ip, port,);
                //        }
                //    }
                //}

                //callback(lobbiesArray.ToList());
                return lobbiesArray;
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
            Debug.Log("Error fetching multiplayer lobbies");
            return null;
        }
    }

    public void LoadLobbies()
    {
        foreach (Lobby lobby in lobbies)
        {
            GameObject lobbyRow = Instantiate(lobbyRowPrefab, listObject);
            lobbyRow.GetComponent<LobbyRow>().LoadLobby(lobby);
            lobbyRow.GetComponent<Button>().onClick.AddListener(delegate { OnLobbySelected?.Invoke(lobby); });
        }
    }
}
