using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using TMPro;
using Steamworks.Data;
using Mirror;
using System;
using System.Linq;

public struct Slot
{
    public SteamId takenBy;
    public bool isConnected;
    public bool isReady;
    public bool isAvailable { get; private set; }
    public void Init()
    {
        takenBy = 0;
        isReady = false;
        isAvailable = true;
    }
    public void Seat(SteamId memberId)
    {
        takenBy = memberId;
        isReady = false;
        isAvailable = false;
    }

    public void Leave()
    {
        takenBy = (SteamId)0;
        isReady = false;
        isAvailable = true;
    }
}

public class LobbyManager : MonoBehaviour
{
    private static LobbyManager instance;
    public static LobbyManager Instance
    {
        get
        {
            return instance;
        }
        private
        set
        {
            instance = value;
        }
    }
    public Lobby currentLobby;
    public ulong lobbyId;
    public LevelAsset currentLevel;
    public Slot[] slots;
    public bool isLobbyReady;
    public Dictionary<SteamId, SteamProfile> profiles;

    public delegate void LobbyUpdate(Lobby lobby);
    public static LobbyUpdate OnLobbyUpdateEvent;
    public delegate void LobbyEntered(Lobby lobby);
    public static LobbyEntered OnLobbyEnteredEvent;
    public delegate void LobbyLeave();
    public static LobbyLeave OnLobbyLeaveEvent;
    public delegate void SlotLeave(int slotIndex);
    public static SlotLeave OnSlotLeave;

    public int connectionAttemp = 0;

    private void Awake()
    {
        Cursor.visible = true;
        if (instance)
        {
            Debug.Log("LobbyManager is already instanced (destroy duplicate instance)");
            Destroy(gameObject);
            return;
        }
        instance = this;
        SteamMatchmaking.OnLobbyMemberDataChanged += OnLobbyMemberDataChanged;
        SteamMatchmaking.OnLobbyEntered += OnLobbyEntered;
        SteamMatchmaking.OnLobbyMemberJoined += OnLobbyMemberJoined;
        SteamMatchmaking.OnLobbyMemberLeave += OnLobbyMemberLeave;
        SteamMatchmaking.OnLobbyDataChanged += OnLobbyDataChanged;
        SteamMatchmaking.OnLobbyCreated += OnLobbyCreated;
        // Join lobby via friend
        SteamFriends.OnGameLobbyJoinRequested += (Lobby lobby, SteamId lobbyId) => {
            if (LobbyManager.Instance.currentLobby.Id != 0)
            {
                LobbyManager.Instance.currentLobby.Leave();
            }
            _ = lobby.Join();
        };
        DontDestroyOnLoad(gameObject);
    }

    private void OnLobbyCreated(Result result, Lobby lobby)
    {
        if (result != Result.OK)
        {
            Debug.LogError("Lobby creation failed");
            Debug.LogError(result.ToString());
            // TODO
            // Handle Lobby creation Fail

        }
        else
        {
            lobby.SetData("startGame", (false).ToString());
        }
    }

    private void OnLobbyEntered(Lobby lobby)
    {
        // Set current Lobby
        currentLobby = lobby;
        lobbyId = currentLobby.Id;
        currentLevel = LevelAssetsLoader.GetLevelAsset(lobby.GetData("level"));

        // Init slots
        slots = new Slot[currentLevel.TotalCount];
        for (int index = 0; index < slots.Length; index++) slots[index].Init();

        profiles = new Dictionary<SteamId, SteamProfile>();

        //Add steamClient Profile
        if (!profiles.ContainsKey(SteamManager.Instance.localPlayer.steamId))
            AddProfile(SteamManager.Instance.localPlayer.steamId, SteamManager.Instance.localPlayer.steamName);

        //Load current member of lobby
        LoadSlots(lobby);

        // Check slots available
        int i = 0;
        while (i < slots.Length)
        {
            if (slots[i].takenBy == SteamClient.SteamId)
            {
                OnLobbyEnteredEvent?.Invoke(lobby);
                return;
            }
            i++;
        }
        i = 0;
        while (i < slots.Length)
        {
            if (slots[i].isAvailable)
            {
                break;
            }
            i++;
        }

        if (i < currentLevel.TotalCount)
        {
            lobby.SetMemberData("slotIndex", i.ToString());
            lobby.SetMemberData("readyState", (false).ToString());
            lobby.SetMemberData("isConnected", (false).ToString());
        }
        else
        {
            Debug.LogError($"Local SteamClient \"{SteamClient.Name}\" (id: {SteamClient.SteamId}) didn't find a slot available => leaving this lobby");
            lobby.Leave();
        }
        OnLobbyEnteredEvent?.Invoke(lobby);
    }

    private void OnLobbyMemberJoined(Lobby lobby, Friend member)
    {
        AddProfile(member.Id, member.Name);
        OnLobbyUpdateEvent?.Invoke(lobby);
    }

    private void OnLobbyMemberLeave(Lobby lobby, Friend member)
    {
        int i = 0;
        while (i < slots.Length)
        {
            if (slots[i].takenBy == member.Id) break;
            i++;
        }
        if (i < slots.Length)
        {
            // Remove From Room
            slots[i].Leave();
            profiles.Remove(member.Id);
            OnSlotLeave?.Invoke(i);
        }
        else
        {
            Debug.LogWarning("Member leave not found in slots");
        }

        if (lobby.Owner.Id == member.Id)
        {
            Debug.Log("the member who just leave was the owner");
        }
        else
        {
            if (lobby.Owner.Id == SteamClient.SteamId)
            {
                lobby.SetMemberData("readyState", (false).ToString());
                Debug.Log("You are the leader of this lobby");
            }
        }
        OnLobbyUpdateEvent?.Invoke(lobby);
    }

    private void OnLobbyDataChanged(Lobby lobby)
    {
        bool readyState = bool.Parse(lobby.GetMemberData(new Friend(SteamClient.SteamId), "readyState"));
        if (lobby.Owner.Id != SteamClient.SteamId && bool.Parse(lobby.GetData("startGame")) && !NetworkClient.active && readyState)
        {
            LoadingScreen.ShowLoadingScreen(() => JoinGame());
        }
    }

    private void OnLobbyMemberDataChanged(Lobby lobby, Friend member)
    {
        //Player Switch slot
        int currentSlot = -1;
        int i = 0;
        while (i < slots.Length)
        {
            if (slots[i].takenBy == member.Id)
            {
                currentSlot = i;
                break;
            }
            i++;
        }
        int newSlot = int.Parse(lobby.GetMemberData(member, "slotIndex"));
        if (currentSlot != newSlot)
        {
            //if new slot is already taken
            if (slots[newSlot].isAvailable)
            {
                if (currentSlot >= 0)
                    slots[currentSlot].Leave();
                slots[newSlot].Seat(member.Id);
                currentSlot = newSlot;
            }
            else
            {
                //lobby.SetMemberData("slotIndex", currentSlot.ToString());
            }
        }

        //Player Switch Ready state
        //TODO Check if member ReadyState data is valid
        slots[currentSlot].isReady = bool.Parse(lobby.GetMemberData(member, "readyState"));
        slots[currentSlot].isConnected = bool.Parse(lobby.GetMemberData(member, "isConnected"));

        bool lobbyReady = true;
        foreach (Slot slot in slots)
        {
            if (slot.takenBy != currentLobby.Owner.Id && !slot.isAvailable && lobbyReady)
                lobbyReady = slot.isReady;
        }
        isLobbyReady = lobbyReady;
        if (lobby.Owner.Id == SteamClient.SteamId)
            SetRoleLeftFlag();
        OnLobbyUpdateEvent?.Invoke(lobby);
    }

    private void AddProfile(SteamId id, string name)
    {
        SteamProfile profile = SteamProfile.Create(id, name);
        profiles.Add(id, profile);
    }

    public void LeaveLobby()
    {
        currentLobby.Leave();
        OnLobbyLeaveEvent?.Invoke();
    }

    public void StartHostGame()
    {
        CustomNetworkManager.singleton.onlineScene = currentLevel.sceneField;
        CustomNetworkManager.singleton.maxConnections = currentLevel.TotalCount;
        CustomNetworkManager.singleton.StartHost();
    }

    public void JoinGame()
    {
        // Set Host Id
        CustomNetworkManager.singleton.networkAddress = currentLobby.Owner.Id.ToString();
        // Set Level scene
        CustomNetworkManager.singleton.onlineScene = currentLevel.sceneField;
        CustomNetworkManager.singleton.StartClient();
    }

    public void EndGame()
    {
        if (NetworkServer.active)
        {
            currentLobby.SetData("gameStart", (false).ToString());
            currentLobby.SetGameServer(0);
        }

        currentLobby.SetMemberData("readyState", (false).ToString());
    }

    public void OnJoinLobbyFailed(RoomEnter reason)
    {
        Debug.LogError($"You fail to join the room because {reason}");
    }

    public void LoadSlots(Lobby lobby)
    {
        // Init slots
        bool lobbyReady = true;
        slots = new Slot[currentLevel.TotalCount];
        for (int index = 0; index < slots.Length; index++) slots[index].Init();
        foreach (Friend member in lobby.Members)
        {
            string slotIndexData = lobby.GetMemberData(member, "slotIndex");
            if (slotIndexData != "")
            {
                int slotIndex = int.Parse(slotIndexData);
                // is valid slot index
                if (slotIndex < currentLevel.TotalCount)
                {
                    slots[slotIndex].Seat(member.Id);
                    slots[slotIndex].isReady = bool.Parse(lobby.GetMemberData(member, "readyState"));
                    if (!profiles.ContainsKey(member.Id))
                        AddProfile(member.Id, member.Name);

                    if (slots[slotIndex].takenBy != currentLobby.Owner.Id && !slots[slotIndex].isAvailable && lobbyReady)
                        lobbyReady = slots[slotIndex].isReady;
                }
                else
                {
                    Debug.LogError($"Lobby Member \"{member.Name}\" (id: {member.Id}) has a invalid slot index value");
                }
            }
            else
            {
                if (member.Id == SteamClient.SteamId)
                    continue;
                else
                    Debug.LogError($"Lobby Member \"{member.Name}\" (id: {member.Id}) is missing a slot index value");
            }
        }
        isLobbyReady = lobbyReady;
    }
    public void ReconnectAttempt()
    {
        connectionAttemp++;
        new Lobby(lobbyId).Join();
        LoadingScreen.ShowLoadingScreen(() => CustomNetworkManager.singleton.StartClient());
    }

    public Friend GetMemberFromSteamId(SteamId steamId)
    {
        return currentLobby.Members.FirstOrDefault(member => member.Id == steamId);
    }
    public Role GetRole(SteamId steamId)
    {
        Friend member = GetMemberFromSteamId(steamId);
        if(member.Id != 0)
            return GetRole(member);
        else
            return Role.Error;
    }
    public Role GetRole(Friend member)
    {
        int slotIndex = GetSlotIndex(member);
        if(slotIndex >= 0)
        {
            if (slotIndex < currentLevel.AssitantCount)
            {
                return Role.Assistant;
            }
            else if (slotIndex < currentLevel.TotalCount)
            {
                return Role.Agent;
            }
        }
        return Role.Error;
    }

    public int GetSlotIndex(Friend member)
    {
        string slotIndexString = currentLobby.GetMemberData(member, "slotIndex");
        if (!string.IsNullOrEmpty(slotIndexString))
        {
            int slotIndex;
            if (int.TryParse(slotIndexString, out slotIndex))
            {
                return slotIndex;
            }
        }
        return -1;
    }

        public void SetConnected(bool state)
    {
        slots[int.Parse(currentLobby.GetMemberData(new Friend(SteamClient.SteamId), "slotIndex"))].isConnected = state;
        LobbyManager.Instance.currentLobby.SetMemberData("isConnected", state.ToString());
    }


    public void SetRoleLeftFlag()
    {
        int countAgent = 0;
        int countAssistant = 0;
        //int countMax = role == Role.Assistant ? lobbyLevel.AssitantCount : lobbyLevel.AgentCount;
        foreach (Friend member in currentLobby.Members)
        {
            string slotIndexData = currentLobby.GetMemberData(member, "slotIndex");
            if (!string.IsNullOrEmpty(slotIndexData))
            {
                int slotIndex;
                if (int.TryParse(slotIndexData, out slotIndex))
                {
                    if (slotIndex < currentLevel.AssitantCount)
                    {
                        //it's a assistant
                        countAssistant++;
                    }
                    else
                    {
                        //it's a agent
                        countAgent++;
                    }
                }
            }
        }
        currentLobby.SetData("AgentSlotAvailable", (countAgent < currentLevel.AgentCount).ToString());
        currentLobby.SetData("AssistantSlotAvailable", (countAssistant < currentLevel.AssitantCount).ToString());
    }

    public void OnDestroy()
    {
        if (this == instance)
        {
            SteamClient.Shutdown();
        }
    }
}
