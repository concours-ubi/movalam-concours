using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GetLevelInfo : MonoBehaviour
{
    public TextMeshProUGUI difficulty;
    public TextMeshProUGUI description;
    public TextMeshProUGUI levelName;
    public Image levelImage;
}
