using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{
    private static LoadingScreen instance;
    private Animator animator;
    private Action callback;
    // Start is called before the first frame update
    void Awake()
    {
        if (!instance)
        {
            instance = this;
            animator = GetComponent<Animator>();
            DontDestroyOnLoad(gameObject);
        }
    }

    public static void ShowLoadingScreen(Action callback)
    {
        if (instance)
        {
            instance.callback = callback;
            instance.animator.SetBool("Visible", true);
        }
        else
        {
            callback?.Invoke();
        }
    }

    public static void HideLoadingScreen(Action callback)
    {
        if (instance)
        {
            instance.callback = callback;
            instance.animator.SetBool("Visible", false);
        }
        else
        {
            callback?.Invoke();
        }
    }

    public void TriggerCallback()
    {
        callback?.Invoke();
    }
}
