using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutocloseTerminal : MonoBehaviour
{
    public void ShouldClose()
    {
        if (transform.parent.name == "DetectorTerminal" || transform.parent.name == "CameraTerminal")
        {
            GetComponentInParent<GenericTerminal>().EndInteraction();
        }
    }
}
