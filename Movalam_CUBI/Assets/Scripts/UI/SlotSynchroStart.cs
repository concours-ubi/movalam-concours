using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SlotSynchroStart : MonoBehaviour
{
    public bool loaded;
    public Image playerAvatar;
    public Image characterSprite;
    public TextMeshProUGUI playerName;
    public TextMeshProUGUI playerStatus;
}
