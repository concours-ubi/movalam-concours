using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Heavely modifier version of https://sushanta1991.blogspot.com/2016/12/how-to-create-carousel-view-with-unity.html

public class CarouselView : MonoBehaviour
{
    //public AnimationCurve transitionCurve;
    public EasingFunction.Ease ease;
    private EasingFunction.Function easeFunction;
    public float transitionTime;

    public bool useScaling;
    public float scaleDownSize;
    public float ScaleAtDistance;

    public bool useFadding;
    public Color faddingColor;

    public List<RectTransform> images;
    public List<Image> imagesFadding;

    private bool canSwipe;
    private float image_width;
    private float lerpTimer;
    private float lerpPosition;
    private float mousePositionStartX;
    private float mousePositionEndX;
    private float dragAmount;
    private float screenPosition;
    private float lastScreenPosition;
    /// <summary>
    /// Space between images.
    /// </summary>
    public float imageGap = 30;

    public int swipeThrustHold = 30;
    [HideInInspector]
    /// <summary>
    /// The index of the current image on display.
    /// </summary>
    public int current_index;

    public Text titel;

    #region mono
    // Use this for initialization
    void Start()
    {
        easeFunction = EasingFunction.GetEasingFunction(ease);
        images = new List<RectTransform>();
        if(useFadding)
            imagesFadding = new List<Image>();
        foreach (Transform element in transform)
        {
            images.Add(element.GetComponent<RectTransform>());
            if (useFadding)
            {
                GameObject fadder = new GameObject("Fadder");
                fadder.transform.SetParent(element);
                RectTransform rt = fadder.AddComponent<RectTransform>();
                rt.localPosition = Vector3.zero;
                rt.localScale = Vector3.one;
                rt.anchorMin = Vector2.zero;
                rt.anchorMax = Vector2.one;
                rt.sizeDelta = Vector2.zero;
                imagesFadding.Add(fadder.gameObject.AddComponent<Image>());
                imagesFadding[imagesFadding.Count - 1].color = Color.clear;
                imagesFadding[imagesFadding.Count - 1].raycastTarget = false;
            }
        }

        //image_width = view_window.rect.width;
        image_width = useScaling ? images[0].rect.width * scaleDownSize : images[0].rect.width;
        for (int i = 1; i < images.Count; i++)
        {
            images[i].anchoredPosition = new Vector2(((image_width + imageGap) * i), 0);
        }
        GoToIndex(0);
    }

    // Update is called once per frame
    void Update()
    {

        //titel.text = current_index.ToString();

        if (lerpTimer < transitionTime)
        {
            lerpTimer = lerpTimer + Time.deltaTime;
            float ratio = lerpTimer / transitionTime;
            //float transitionRatio = transitionCurve.Evaluate(ratio);
            //screenPosition = Mathf.Lerp(lastScreenPosition, lerpPosition * -1, transitionRatio);
            screenPosition = easeFunction(lastScreenPosition, lerpPosition * -1, ratio);
        }
        else
        {
            lerpTimer = transitionTime;
            screenPosition = lerpPosition * -1;
            lastScreenPosition = screenPosition;
        }

        // Start Swipe
        if (Input.GetMouseButtonDown(0))
        {
            canSwipe = true;
            mousePositionStartX = Input.mousePosition.x;
        }

        // Swipe thrusthold
        if (Input.GetMouseButton(0))
        {
            if (canSwipe)
            {
                mousePositionEndX = Input.mousePosition.x;
                dragAmount = mousePositionEndX - mousePositionStartX;
                //screenPosition = lastScreenPosition + dragAmount;
            }
        }

        // On Swipe
        if (Mathf.Abs(dragAmount) > swipeThrustHold && canSwipe)
        {

            canSwipe = false;
            lastScreenPosition = screenPosition;
            if (current_index < images.Count)
                OnSwipeComplete();
            else if (current_index == images.Count && dragAmount < 0)
                lerpTimer = 0;
            else if (current_index == images.Count && dragAmount > 0)
                OnSwipeComplete();
        }

        // Set Image Position
        for (int i = 0; i < images.Count; i++)
        {
            images[i].anchoredPosition = new Vector2(screenPosition + ((image_width + imageGap) * i), 0);
            float distFromCenter = Mathf.Abs(images[i].anchoredPosition.x);
            // Set Image Scale
            if (useScaling)
            {
                float scale = Mathf.Lerp(1.0f, scaleDownSize, distFromCenter / ScaleAtDistance);
                images[i].localScale = new Vector3(scale, scale, scale);
            }

            if (useFadding)
            {
                Color fadded = Color.Lerp(Color.clear, faddingColor, distFromCenter / ScaleAtDistance);
                imagesFadding[i].color = fadded;
            }
        }
    }

    void OnValidate()
    {
        easeFunction = EasingFunction.GetEasingFunction(ease);
    }
    #endregion


    #region private methods
    void OnSwipeComplete()
    {
        lastScreenPosition = screenPosition;

        if (dragAmount > 0)
        {
            if (dragAmount >= swipeThrustHold)
            {
                if (current_index == 0)
                {
                    lerpTimer = 0; lerpPosition = 0;
                }
                else
                {
                    current_index--;
                    lerpTimer = 0;
                    if (current_index < 0)
                        current_index = 0;
                    lerpPosition = (image_width + imageGap) * current_index;
                }
            }
            else
            {
                lerpTimer = 0;
            }
        }
        else if (dragAmount < 0)
        {
            if (Mathf.Abs(dragAmount) >= swipeThrustHold)
            {
                if (current_index == images.Count - 1)
                {
                    lerpTimer = 0;
                    lerpPosition = (image_width + imageGap) * current_index;
                }
                else
                {
                    lerpTimer = 0;
                    current_index++;
                    lerpPosition = (image_width + imageGap) * current_index;
                }
            }
            else
            {
                lerpTimer = 0;
            }
        }
        dragAmount = 0;
    }
    #endregion


    #region public methods
    public void GoToIndex(int value)
    {
        current_index = value;
        lerpTimer = 0;
        lerpPosition = (image_width + imageGap) * current_index;
        screenPosition = lerpPosition * -1;
        lastScreenPosition = screenPosition;
        for (int i = 0; i < images.Count; i++)
        {
            images[i].anchoredPosition = new Vector2(screenPosition + ((image_width + imageGap) * i), 0);
        }
    }

    public void GoToIndexSmooth(int value)
    {
        lastScreenPosition = screenPosition;
        current_index = value;
        lerpTimer = 0;
        lerpPosition = (image_width + imageGap) * current_index;
    }

    public void Previous()
    {
        int index = current_index > 0 ? current_index - 1 : 0;
        GoToIndexSmooth(index);
    }

    public void Next()
    {
        int index = current_index < images.Count - 1 ? current_index + 1 : images.Count - 1;
        GoToIndexSmooth(index);
    }
    #endregion
}
