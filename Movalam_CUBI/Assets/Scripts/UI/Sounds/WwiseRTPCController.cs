using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AK.Wwise;
using TMPro;

[RequireComponent(typeof(Slider))]
public class WwiseRTPCController : MonoBehaviour
{
    public string key;
    public RTPC rtpc;
    public float minValue = 0;
    public float maxValue = 100;
    public float value
    {
        get
        {
            return slider.value;
        }
        set
        {
            slider.value = value;
            rtpc.SetGlobalValue(value);
            valueText.text = value.ToString("#.");
            PlayerPrefs.SetFloat(key, value);
        }
    }
    private Slider slider;
    public TextMeshProUGUI valueText;

    private void Awake()
    {
        if (rtpc == null) return;
        slider = GetComponent<Slider>();
        slider.minValue = minValue;
        slider.maxValue = maxValue;
        value = PlayerPrefs.HasKey(key) ? PlayerPrefs.GetFloat(key) : rtpc.GetGlobalValue();
        slider.onValueChanged.AddListener(value => this.value = value);
    }

    public void lessVolume()
    {
        value = Mathf.Clamp(value - 10, 0, 100);
    }

    public void moreVolume()
    {
        value = Mathf.Clamp(value + 10, 0, 100);
    }
}
