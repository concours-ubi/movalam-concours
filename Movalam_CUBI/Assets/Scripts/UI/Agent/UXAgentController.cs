using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UXAgentController : MonoBehaviour
{
    [SerializeField] GameObject playerBannerPrefab;         // R�f�rence sur le prefab banner
    public Transform playerPanel;                           // Transform du group des banners

    public PlayerBannerController AddPlayerBannerController(string name, Sprite avatar)
    {
        GameObject banner;
        banner = Instantiate(playerBannerPrefab, playerPanel);
        PlayerBannerController playerBanner = banner.GetComponent<PlayerBannerController>();
        playerBanner.SetPlayerBanner(name, avatar);
        return playerBanner;
    }
}
