using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [Header("Cursors")]
    [SerializeField] private Transform horizontalCursorBar;                 // R�f�rence sur la barre horizontale du curseur
    [SerializeField] private Transform verticalCursorBar;                   // R�f�rence sur la barre verticale du curseur

    private bool check = true;

    private void Update()
    {
        if (check)
        {
            Cursor.visible = false;
            verticalCursorBar.position = new Vector3(Input.mousePosition.x, verticalCursorBar.position.y, verticalCursorBar.position.z);
            horizontalCursorBar.position = new Vector3(horizontalCursorBar.position.x, Input.mousePosition.y, horizontalCursorBar.position.z);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
