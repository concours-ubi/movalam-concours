using UnityEngine;
using UnityEngine.EventSystems;


// If there is no selected item, set the selected item to the event system's first selected item
public class ControllerRefocus : MonoBehaviour
{
    private GameObject lastSelected;
    private GameObject currentSelectedGameObject_Recent;

    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject != currentSelectedGameObject_Recent)
        {
            lastSelected = currentSelectedGameObject_Recent;
            currentSelectedGameObject_Recent = EventSystem.current.currentSelectedGameObject;
        }
        if (EventSystem.current.currentSelectedGameObject == null)
        {
            EventSystem.current.SetSelectedGameObject(lastSelected);
        }
    }
}
