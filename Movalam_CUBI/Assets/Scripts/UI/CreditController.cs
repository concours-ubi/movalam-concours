using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditController : MonoBehaviour
{
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject creditMenu;

    void OnEnable()
    {
        GetComponentInChildren<Animator>().Play("Start");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ReturnToMenu();
    }

    public void ReturnToMenu()
    {
        creditMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
}
