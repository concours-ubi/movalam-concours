using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlgorithmCardController : MonoBehaviour
{
    [SerializeField] private Emote[] emoteList;                         // La liste des emotes que la carte peut �tre
    private List<Emote> emoteListWithWeigths = new List<Emote>();       // La liste dans laquelle piocher qui prend en compte les probabilit�s
    private List<Emote> emoteInUse = new List<Emote>();                 // La liste des emotes actuellement pr�sentes
    private Emote emoteToUse;                                           // Une emote tampon qui sera envoy�e lorsqu'on demande une nouvelle emote

    [Header("Algorithm options")]
    [SerializeField] private int maxNumberOfUseless;                    // Le nombre max d'emote useless
    private int numberOfUselessEmote = 0;                               // Le nombre actuel d'emote useless
    [SerializeField] private int maxRandomIteration;                    // Le nombre d'it�ration en random avant de parcourir pour trouver une solution

    [Header("Cards")]
    [SerializeField] private CardController[] cards;                    // Les scripts des cartes pour les initialiser

    /*private void Start()
    {
        foreach(Emote e in emoteList)
        {
            AddEmoteInList(e);
        }

        foreach (CardController cc in cards)
        {
            cc.ChangeEmote();
        }
    }

    public Emote ChangeEmote(Emote usedEmote)
    {
        bool goOn = false;
        int iteration = 0;

        while (iteration < maxRandomIteration && !goOn)
        {
            emoteToUse = emoteListWithWeigths[Random.Range(0, emoteListWithWeigths.Count)];

            if (IsValidEmote(emoteToUse))
                goOn = true;
        }
        if(!goOn)
        {
            int iterator = 0;
            while(!goOn)
            {
                emoteToUse = emoteListWithWeigths[iterator];

                if (IsValidEmote(emoteToUse))
                    goOn = true;

                iterator++;
            }
        }

        if(usedEmote != null)
        {
            if (!usedEmote.useful)
                numberOfUselessEmote--;
            emoteInUse.Remove(usedEmote);
        }

        if (!emoteToUse.useful)
            numberOfUselessEmote++;
        emoteInUse.Add(emoteToUse);

        return emoteToUse;
    }

    private void AddEmoteInList(Emote emote)
    {
        for(int i = 0; i < emote.weight; i++)
        {
            emoteListWithWeigths.Add(emote);
        }
    }

    private bool IsValidEmote(Emote emote)
    {
        if (numberOfUselessEmote == maxNumberOfUseless && !emote.useful)
            return false;

        if (emoteInUse.Contains(emote))
            return false;

        return true;
    }

    public void AdjustDifficulty()
    {
        foreach (Emote e in emoteList)
        {
            if(e.useful)
            {
                emoteListWithWeigths.Add(e);
            }
        }
    }*/
}
