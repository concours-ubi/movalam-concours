using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardController : MonoBehaviour
{
    [SerializeField] private Emote actualEmote;                                   // L'emote actuelle

    [SerializeField] private Image emoteImage;                          // Une r�f�rence sur le game object contenant l'image de l'emote

    //private int utilisationLeft = 0;                                    // Le nombre d'utilisation restant de l'emote actuelle

    //private Slider usesBar;                                             // Le slider qui correspond � la barre repr�sentant le nombre d'utilisations restantes
    [SerializeField] private Image fillSlider;                          // La couleur de la barre d'utilisation

    public OverheatBarController overheatBar;                          // Une r�f�rence sur la barre de surchauffe
    public LocalAssistant assistantController;                         // Une r�f�rence sur le script de l'assistant

    private Color red = new Color(247 / 255f, 107 / 255f, 99 / 255f, 1);
    private Color blue = new Color(132 / 255f, 237 / 255f, 247 / 255f, 1);

    //[SerializeField] private AlgorithmCardController algorithm;         // R�f�rence sur le script pour demander de nouvelles emotes



    void Start()
    {
        //usesBar = GetComponentInChildren<Slider>();
        //usesBar.minValue = 0;

        //overheatBar = GameObject.FindGameObjectWithTag("Overheat").GetComponent<OverheatBarController>();
        //assistantController = GameObject.FindGameObjectWithTag("Assistant").GetComponent<LocalAssistant>();

        emoteImage.sprite = actualEmote.sprite;
    }

    public void GetClicked()
    {
        if (overheatBar.CanSendEmote())
        {
            overheatBar.SendEmote();
            assistantController.SendEmote(actualEmote);

            /*utilisationLeft -= 1;

            if (utilisationLeft <= 0)
            {
                ChangeEmote();
            }
            usesBar.value = utilisationLeft;
            if (utilisationLeft == 1)
            {
                fillSlider.color = red;
            }*/
        }
    }

    /*public void ChangeEmote()
    {
        actualEmote = algorithm.ChangeEmote(actualEmote);
        utilisationLeft = actualEmote.numberUtilisation;
        emoteImage.sprite = actualEmote.sprite;

        if (usesBar == null)
            usesBar = GetComponentInChildren<Slider>();

        usesBar.maxValue = actualEmote.numberUtilisation;
        usesBar.value = utilisationLeft;
        if (utilisationLeft == 1)
        {
            fillSlider.color = red;
        }
        else
        {
            fillSlider.color = blue;
        }
    }*/

    /* private void AddMarker(int nbUses)
     {
         RectTransform rt = (RectTransform)usesBar.transform;
         float width = rt.rect.width;

         for (int i = 1; i < nbUses; i++)
         {
             float xPosition = width * i / (float)nbUses;
         }
     }*/
}
