using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Steamworks;
using Steamworks.Data;
using System.Linq;

public class QuickPlayInterface : MonoBehaviour
{
    public GameObject roleChoice;
    public GameObject Searching;
    public GameObject noLobbyFound;

    public void QuickPlayAssistant()
    {
        QuickPlay(Role.Assistant);
    }
    public void QuickPlayAgent()
    {
        QuickPlay(Role.Agent);
    }
    public async void QuickPlay(Role role)
    {
        roleChoice.SetActive(false);
        Searching.SetActive(true);
        Lobby quickLobby = await FindLobbyWithRoleSlotLeft(role);
        Searching.SetActive(false);
        if (quickLobby.Id != 0)
        {
            gameObject.SetActive(false);
            _ = quickLobby.Join();
        }
        else
        {
            //Debug.Log("Aucun lobby trouv� pour se role");
            noLobbyFound.SetActive(true);
        }
    }

    public async Task<Lobby> FindLobbyWithRoleSlotLeft(Role wantedRole)
    {
        Lobby[] lobbies = await ListLobbies.GetLobbyList();
        if (lobbies != null)
        {
            foreach (Lobby lobby in lobbies)
            {
                string flagName = wantedRole == Role.Assistant ? "AssistantSlotAvailable" : "AgentSlotAvailable";
                bool flagData = bool.Parse(lobby.GetData(flagName));
                if (flagData)
                    return lobby;
            }
        }
        return new Lobby();
    }

    public async Task<List<Lobby>> FindLobbiesWithRoleSlotLeft(Role wantedRole)
    {
        List<Lobby> lobbies = (await ListLobbies.GetLobbyList()).ToList();
        if (lobbies != null)
        {
            for (int i = 0; i < lobbies.Count; i++)
            {
                string flagName = wantedRole == Role.Assistant ? "AssistantSlotAvailable" : "AgentSlotAvailable";
                bool flagData = bool.Parse(lobbies[i].GetData(flagName));
                if (!flagData)
                {
                    lobbies.RemoveAt(i);
                    i--;
                }
            }
        }
        return lobbies;
    }
}
