using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;
using Mirror;
using Steamworks.Data;

public class SynchroStart : NetworkBehaviour
{
    public GameObject AssistantsSlots;
    public GameObject AgentsSlots;
    public GameObject slotPrefab;

    public Button startButton;
    public GameObject OwnerWaitingLabel;

    public Sprite emptyAvatar;
    public Sprite[] characters;
    public Canvas synchroStartCanvas;
    private SlotSynchroStart[] slots;

    public delegate void GameStarted();
    public static GameStarted OnGameStarted; 

    private void Start()
    {
        if (!CustomNetworkManager.singleton.isNetworkActive) StartGame();
        //check syncStart Done
        string syncStart = LobbyManager.Instance.currentLobby.GetData("syncStart");

        if (string.IsNullOrEmpty(syncStart)) StartGame();
        if (bool.Parse(syncStart)) StartGame();
        LevelAsset level = LobbyManager.Instance.currentLevel;
        slots = new SlotSynchroStart[level.TotalCount];

        // Init Slots
        for (int i = 0; i < level.TotalCount; i++)
        {
            Transform folder = i < level.AssitantCount ? AssistantsSlots.transform : AgentsSlots.transform;
            CreateSlot(i, folder);
            if (!LobbyManager.Instance.slots[i].isAvailable)
            {
                Friend member = new Friend(LobbyManager.Instance.slots[i].takenBy);
                UpdateSlot(LobbyManager.Instance.currentLobby, member);
            }
        }


        if (SteamClient.SteamId == LobbyManager.Instance.currentLobby.Owner.Id)
        {
            startButton.gameObject.SetActive(true);
            startButton.onClick.AddListener(() => {
                LobbyManager.Instance.currentLobby.SetData("syncStart", (true).ToString()); ;
                ClientRPCStartGame();
            });
        }
        else
        {
            OwnerWaitingLabel.SetActive(true);
        }
        LobbyManager.OnSlotLeave += UnloadSlot;
        SteamMatchmaking.OnLobbyMemberDataChanged += UpdateSlot;
    }

    public void UnloadSlot(int slotIndex)
    {
        slots[slotIndex].playerName.text = "Empty";
        slots[slotIndex].playerAvatar.sprite = emptyAvatar;
        slots[slotIndex].playerAvatar.transform.localScale = new Vector3(1, 1, 1);
        slots[slotIndex].playerStatus.text = "";
        slots[slotIndex].loaded = false;
        slots[slotIndex].characterSprite.color = new UnityEngine.Color(43, 43, 43);
    }

    public void LoadSlot(Lobby lobby, Friend member)
    {
        SteamProfile profile = LobbyManager.Instance.profiles[member.Id];
        int slotIndex = int.Parse(lobby.GetMemberData(member, "slotIndex"));
        slots[slotIndex].playerName.text = profile.steamName;
        slots[slotIndex].playerAvatar.sprite = profile.steamAvatar;
        slots[slotIndex].playerAvatar.transform.localScale = new Vector3(1, -1, 1);
        slots[slotIndex].playerStatus.text = "Connecting";
        slots[slotIndex].loaded = true;
    }

    public void UpdateSlot(Lobby lobby, Friend member)
    {
        int slotIndex = int.Parse(lobby.GetMemberData(member, "slotIndex"));
        if (!slots[slotIndex].loaded)
            LoadSlot(lobby, member);
        bool isConnected = bool.Parse(lobby.GetMemberData(member, "isConnected"));
        if (isConnected)
        {
            slots[slotIndex].playerStatus.text = "Connected";
            slots[slotIndex].characterSprite.color = UnityEngine.Color.white;
        }
    }

    public void CreateSlot(int slotIndex, Transform folder)
    {
        slots[slotIndex] = Instantiate(slotPrefab, folder).GetComponent<SlotSynchroStart>();
        slots[slotIndex].characterSprite.sprite = characters[slotIndex];
    }

    [ClientRpc]
    private void ClientRPCStartGame()
    {
        StartGame();
    }

    private void StartGame()
    {
        synchroStartCanvas.enabled = false;
        OnGameStarted?.Invoke();
    }
}
