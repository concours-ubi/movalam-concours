using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TextWriter : MonoBehaviour
{
    public float delay = 0.1f;                  // Le temps entre chaque caract�re
    private Text textComponent;                 // R�f�rence sur le composant de text
    private int numberAgentsDown = 0;            // Le nombre d'agents morts dans la partie
    private string timeToFinish = "06:15:64";    // Le temps du niveau
    private string levelName = "Medium_04";      // Le nom du niveau

    private string currentText = "";
    private string fullText = "";

    private float startTime;
    private float endTime;

    private int indexAffichage;

    private void Start()
    {
        textComponent = GetComponent<Text>();
        if (CustomNetworkManager.singleton)
            startTime = (float)NetworkTime.time;
        else
            startTime = Time.time;

        // Mettre en r�seau pour r�cup�rer le nom du niveau dans les levelAsset
        levelName = SceneManager.GetActiveScene().name;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            indexAffichage = fullText.Length + 1;
            textComponent.text = fullText;
        }
    }

    public void Initialize()
    {
        if (CustomNetworkManager.singleton)
            endTime = (float)NetworkTime.time;
        else
            endTime = Time.time;

        if(endTime - startTime > 60)
        {
            timeToFinish = (Mathf.Floor((endTime - startTime)/60)).ToString()+ " minutes " + ((endTime - startTime)- Mathf.Floor((endTime - startTime) / 60) * 60).ToString("F2") + " seconds";
        }
        else
            timeToFinish = (endTime - startTime).ToString("F2") + " seconds";

        if(GameObject.FindGameObjectWithTag("AssistantNetwork") != null)
            numberAgentsDown = GameObject.FindGameObjectWithTag("AssistantNetwork").GetComponent<NetworkAssistant>().GetDownCounter();

        fullText += "> Time : " + timeToFinish + "\n\n";
        fullText += "> Level : " + levelName + "\n\n";
        fullText += "> Agents down : " + numberAgentsDown;

        StartCoroutine(ShowText());
    }

    IEnumerator ShowText()
    {
        for (indexAffichage = 0; indexAffichage < fullText.Length+1; indexAffichage++)
        {
            currentText = fullText.Substring(0, indexAffichage);
            textComponent.text = currentText;
            yield return new WaitForSeconds(delay);
        }
    }
}
