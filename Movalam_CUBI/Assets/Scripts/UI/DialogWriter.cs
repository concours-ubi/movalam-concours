using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogWriter : MonoBehaviour
{
    public float delay = 0.05f;                              // Le temps entre chaque caract�re
    private Text textComponent;                             // R�f�rence sur le composant de text

    private string currentText = "";                        // Le texte actuellement affich�
    private List<string> fullText = new List<string>();     // Tout le texte � afficher, page par page
    private string textTampon = "";                         // string utilis� pour remplir fullText

    private int numeroPage = 0;                             // La "page" de texte � afficher
    private int indexAffichage = 0;                         // L'index utilis� dans le for d'affichage du texte
    private bool fullyWriten = false;                       // boolen indiquant si la page actuelle est finie d'�tre �crite

    [SerializeField] Canvas statsMenu;                      // R�f�rence sur le canvas suivant
    [SerializeField] Canvas dialogMenu;                     // R�f�rence sur le canvas actuel

    private bool done = false;

    public void Initialize()
    {
        if (!done)
        {
            done = true;
            textComponent = GetComponent<Text>();
            textTampon = "Congratulations contenders ! You managed to beat this level of Lead Us, the escape game brought to you by MOVALAM Computer.";
            fullText.Add(textTampon);
            textTampon = "You managed to avoid every trap in order to reach your final goal : the prize !";
            fullText.Add(textTampon);
            textTampon = "I am pretty sure that you can not wait to see what it is that you won.";
            fullText.Add(textTampon);
            textTampon = "It was the friendship that you made during the game !";
            fullText.Add(textTampon);

            StartCoroutine(ShowText());
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && dialogMenu.enabled)
        {
            if (!fullyWriten)
            {
                indexAffichage = fullText[numeroPage].Length + 1;
                textComponent.text = fullText[numeroPage];
                fullyWriten = true;
            }
            else
            {
                if (numeroPage == fullText.Count-1)
                {
                    dialogMenu.enabled = false;
                    statsMenu.enabled = true;
                    statsMenu.gameObject.GetComponent<BarCursorControllerEndGame>().Initialize();
                }
                else
                {
                    numeroPage++;
                    fullyWriten = false;
                    StartCoroutine(ShowText());
                }
            }
        }
    }

    IEnumerator ShowText()
    {
        for (indexAffichage = 0; indexAffichage < fullText[numeroPage].Length + 1; indexAffichage++)
        {
            currentText = fullText[numeroPage].Substring(0, indexAffichage);
            textComponent.text = currentText;
            yield return new WaitForSeconds(delay);
        }
        fullyWriten = true;
    }
}
