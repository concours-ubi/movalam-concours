using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverWriter : MonoBehaviour
{
    public float delay = 0.05f;                  // Le temps entre chaque caract�re
    private Text textComponent;                 // R�f�rence sur le composant de text

    private string currentText = "";
    private string fullText = "";

    private int indexAffichage;

    private void Start()
    {
        textComponent = GetComponent<Text>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            indexAffichage = fullText.Length + 1;
            textComponent.text = fullText;
        }
    }

    public void Initialize()
    {
        fullText += "> An agent got captured " + "\n\n";
        fullText += "> : ( " + "\n\n";
        fullText += "> That is so sad, you were so close..." + "\n\n";
        fullText += "> Try again next time" + "\n\n";
        fullText += "> Lead Us, the escape game brought to you by MOVALAM Computer, can not wait to see you again !";

        StartCoroutine(ShowText());
    }

    IEnumerator ShowText()
    {
        for (indexAffichage = 0; indexAffichage < fullText.Length + 1; indexAffichage++)
        {
            currentText = fullText.Substring(0, indexAffichage);
            textComponent.text = currentText;
            yield return new WaitForSeconds(delay);
        }
    }
}
