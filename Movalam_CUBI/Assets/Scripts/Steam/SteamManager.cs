using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Steamworks;
using Steamworks.Data;
using System;
using System.Threading.Tasks;

public class SteamManager : MonoBehaviour
{

    private static SteamManager instance;
    public static SteamManager Instance
    {
        get
        {
            return instance;
        }
        private
        set
        {
            instance = value;
        }
    }

    public int appid;

    public SteamProfile localPlayer;
    //public Lobby currentLobby;


    public TMP_InputField lobbyCreationName;
    public LobbyManager lobbyManager;

    public Transform mainMenu;
    public Transform lobbyCreationInterface;
    public Transform lobbyInterface;
    public Transform lobbiesList;
    public Transform steamErrorMessage;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance)
        {
            Debug.Log("SteamManager is already instanced (destroy duplicate instance)");
            Destroy(gameObject);
            return;
        }

        instance = this;

        try
        {
            if(!SteamClient.IsValid)
                SteamClient.Init((uint)appid);
            //SteamClient.Init(1536520);

            localPlayer = SteamProfile.Create(SteamClient.SteamId, SteamClient.Name);
        }
        catch (System.Exception e)
        {
            Debug.LogWarning("Unable to initializa Steam Client");
            Debug.LogWarning(e);
            mainMenu.gameObject.SetActive(false);
            steamErrorMessage.gameObject.SetActive(true);
            // Something went wrong! Steam is closed?
        }
        DontDestroyOnLoad(gameObject);
    }

    public static IEnumerator GetAvatar(SteamProfile profile)
    {
        Task<Image?> avatarRequest = SteamFriends.GetLargeAvatarAsync(profile.steamId);
        while (!avatarRequest.IsCompleted)
        {
            yield return null;
        }
        if(avatarRequest.Result.HasValue)
            profile.SetAvatar(avatarRequest.Result.Value);  
        yield return null;
    }

    public static async Task<bool> CreateLobby(LobbyParameters lobbyParameters)
    {
        try
        {

            var createLobbyOutput = await SteamMatchmaking.CreateLobbyAsync(lobbyParameters.levelAsset.AssitantCount + lobbyParameters.levelAsset.AgentCount);
            if (!createLobbyOutput.HasValue)
            {
                Debug.Log("Lobby created but not correctly instantiated");
                throw new Exception();
            }

            //LobbyPartnerDisconnected = false;
            createLobbyOutput.Value.SetData("name", lobbyParameters.lobbyName);

            //If still using Spacewar appid set custom data to filter later on search for lobbies
            if (SteamManager.Instance.appid == 480)
            {
                createLobbyOutput.Value.SetData("game", "movaclam");
            }

            createLobbyOutput.Value.SetData("level", lobbyParameters.levelAsset.name);
            createLobbyOutput.Value.SetData("startGame", (false).ToString());
            createLobbyOutput.Value.SetData("syncStart", (false).ToString());
            switch (lobbyParameters.joignabilityLevel)
            {
                case LobbyParameters.privacyLevel.Public:
                    createLobbyOutput.Value.SetPublic();
                    break;
                case LobbyParameters.privacyLevel.FriendsOnly:
                    createLobbyOutput.Value.SetFriendsOnly();
                    break;
                case LobbyParameters.privacyLevel.Private:
                    createLobbyOutput.Value.SetPrivate();
                    break;
                default:
                    break;
            }

            if (lobbyParameters.visibilityLevel)
            {
                createLobbyOutput.Value.SetInvisible();
            }

            createLobbyOutput.Value.SetJoinable(true);
            //hostedMultiplayerLobby.SetData(staticDataString, lobbyParameters)

            return true;
        }
        catch (Exception exception)
        {
            Debug.Log("Failed to create multiplayer lobby");
            Debug.Log(exception.ToString());
            return false;
        }
    }
}
