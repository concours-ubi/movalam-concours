using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using Steamworks.Data;


[System.Serializable]
public class SteamProfile
{
    public delegate void ProfileUpdated(SteamProfile profile);
    public ProfileUpdated OnProfileUpdated;

    public SteamId steamId;
    public string steamName;
    public Sprite steamAvatar;
    public static SteamProfile Create(SteamId steamId, string steamName)
    {

        SteamProfile profile = new SteamProfile(steamId, steamName);
        SteamManager.Instance.StartCoroutine(SteamManager.GetAvatar(profile));
        return profile;
    }
    public static SteamProfile Create(SteamId steamId)
    {
        return Create(steamId, "no name");
    }
    public SteamProfile(SteamId steamId, string steamName)
    {
        this.steamId = steamId;
        this.steamName = steamName;
        this.steamAvatar = null;
    }
    public void SetAvatar(Image avatar)
    {
        Texture2D avatarTexture = new Texture2D((int)avatar.Width, (int)avatar.Width, TextureFormat.RGBA32, false);
        avatarTexture.LoadRawTextureData(avatar.Data);
        avatarTexture.Apply();
        steamAvatar = Sprite.Create(avatarTexture, new Rect(0, 0, avatarTexture.width, avatarTexture.height), new Vector2(0f, 1f));
        OnProfileUpdated?.Invoke(this);
    }
};
