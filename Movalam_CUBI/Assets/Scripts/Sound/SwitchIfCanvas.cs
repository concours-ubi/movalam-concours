using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchIfCanvas : MonoBehaviour
{
    public Canvas canvasToBeEnable;
    public AK.Wwise.State gameplayState;
    public AK.Wwise.State borneState;

    private bool done = false;

    void Update()
    {
        if (canvasToBeEnable.enabled)
        {
            borneState.SetValue();
        }
        if (!canvasToBeEnable.enabled && !done)
        {
            done = true;
            gameplayState.SetValue();
        }
    }
}
