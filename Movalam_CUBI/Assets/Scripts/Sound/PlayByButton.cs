using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayByButton : MonoBehaviour
{
    public AK.Wwise.Event wwiseEvent;
    
    public void PlayTheEvent()
    {
        wwiseEvent.Post(gameObject);
    }
}
