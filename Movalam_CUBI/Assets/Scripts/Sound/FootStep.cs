using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootStep : MonoBehaviour
{
    public AK.Wwise.Event wwiseEventFootStep;

    public void playSound(AnimationEvent evt)
    {
        if (evt.animatorClipInfo.weight > 0.5f)
        {
            wwiseEventFootStep.Post(gameObject);
        }
    }
}
