using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class DecodeurPorte : GenericTerminal
{
    [Header("Door specificities")]
    [SerializeField] private Animator m_Animator;                           // L'animator de la porte � ouvrir
    [SerializeField] private GameObject objectToEnable;                     // Une r�f�rence sur le GameObject
    public float percentageDetection = 0.6f;              // Detection � ajouter au joueur si le code rentr� n'est pas le bon

    [Header("Keypad Settings")]
    public string curPassword = "123";                                      // Le mot de passe pr�sent
    [SerializeField] private string input;                                  // Nombre qui a �t� rentr�

    // Local private variables
    private DetectionController detectionScript;                            // R�f�rence sur le script de d�tection du joueur
    private bool alreadyDetected = false;                                    // Si le joueur a d�j� rat� le mdp avec cette combinaison

    public delegate void OpenDoorEvent();
    public OpenDoorEvent OnOpenDoorEvent;
    public delegate void FailedPassword();
    public FailedPassword OnFailedPassword;

    private bool opened = false;

    public override void Update()
    {
        if(!opened)
        {
            base.Update();
            if (Input.GetKeyDown(KeyCode.R))
            {
                input = "";
            }
        }
    }

    public void Confirm()
    {
        if (input == curPassword)
        {
            OuvrirPorte();
            OnOpenDoorEvent?.Invoke();
        }
        else
        {
            Detected();
            OnFailedPassword?.Invoke();
        }
        EndInteraction();
    }

    public void ValueEntered(string valueEntered)
    {
        alreadyDetected = false;
        switch (valueEntered)
        {
            case "Q": // QUIT
                objectToEnable.SetActive(false);
                input = "";
                break;

            default: // Buton clicked add a variable
                input += valueEntered;
                break;
        }
    }

    public override void OnTriggerEnter(Collider other)
    {
        if(!opened)
        {
            base.OnTriggerEnter(other);
            if (other.CompareTag("Player"))
            {
                detectionScript = other.gameObject.GetComponent<DetectionController>();
            }
        }
    }

    public override void OnTriggerExit(Collider other)
    {
        if(!opened)
        {
            base.OnTriggerExit(other);
            if (other.CompareTag("Player"))
            {
                detectionScript = null;
            }
        }
    }

    public override void EndInteraction()
    {
        alreadyDetected = false;
        base.EndInteraction();
        input = "";
    }

    private void Detected()
    {
        if (alreadyDetected == false)
        {
            detectionScript.AddDetection(percentageDetection, false, true);
            alreadyDetected = true;
        }
    }

    public void OuvrirPorte()
    {
        opened = true;
        objectToEnable.SetActive(false);
        m_Animator.SetBool("Open", true);
        if(interactionMessage)
            SetInteractionMessage("");
        if(interactionAgentPanel)
            this.interactionAgentPanel.SetActive(false);
        //this.enabled = false;
    }

    public bool GetOpened()
    {
        return opened;
    }
}
