using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class CameraController : MonoBehaviour
{
    [Header("Detection")]
    [SerializeField] private float maxPercentageDetection;           // Le pourcentage de d�tection ajout� au centre de la zone
    [SerializeField] private float minPercentageDetection;           // Le pourcentage de d�tection ajout� sur le point le plus �loign� du centre de la zone

    [Header("Movement")]
    //[SerializeField] private float speed;                            // La vitesse du detecteur
    [SerializeField] private bool move;                              // Indique si le detecteur doit bouger ou non

    [Header("Positions")]
    [SerializeField] private Transform maxPos;                       // La position de son fils maxPosition    
    [SerializeField] private Transform minPos;                       // La position de son fils minPosition

    private Vector3 target;                                          // L'endroit vers lequel se dirige le detecteur (min ou max pos)
    private Vector3 nonTarget;                                       // l'autre position que target
    private float distanceBetweenTargets;                            // Distance entre les deux points de la cam�ra

    private float distance;                                          // La distance entre le centre de la cam�ra et le joueur
    private float percentageNearCenter;                              // A quel point le joueur est proche du centre
    private SphereCollider sphereCollider;                           // R�f�rence sur le sphere collider

    void Start()
    {
        sphereCollider = GetComponent<SphereCollider>();
        distanceBetweenTargets = Vector3.Distance(maxPos.position, minPos.position);
    }

    void Update()
    {
        if (move)
        {
            // Distance parcourue par la cam�ra depuis le d�but
            float distanceToCatch = Time.time;
            if (CustomNetworkManager.singleton)
                distanceToCatch = (float)NetworkTime.time;// * time;

            // Nombre de fois qu'elle a chang� de target
            int roundTrip = Mathf.FloorToInt(distanceToCatch / distanceBetweenTargets);

            // On choisit la target
            if (roundTrip % 2 == 0)
            {
                target = maxPos.position;
                nonTarget = minPos.position;
            }
            else
            {
                target = minPos.position;
                nonTarget = maxPos.position;
            }

            // On se place sur la position o� la cam�ra est cens� �tre entre les deux targets
            transform.position = nonTarget + (target - nonTarget).normalized * (distanceToCatch - roundTrip * distanceBetweenTargets);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            distance = Vector3.Distance(other.transform.position, transform.position);
            percentageNearCenter = sphereCollider.radius / distance;
            other.gameObject.GetComponentInChildren<DetectionController>().AddDetection(minPercentageDetection + percentageNearCenter*(maxPercentageDetection-minPercentageDetection), true);
        }
    }
}
