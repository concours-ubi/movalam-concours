using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class RoomMaker : MonoBehaviour
{
    [System.Serializable]
    public struct InstantiateDoor
    {
        public int positionX;
        public int positionZ;
    }
    public GameObject tileCenter;
    public GameObject tileBorder;
    public GameObject wall;
    public GameObject door;
    public InstantiateDoor[] doors;
    public int width; //x
    public int height; //z

    
    public bool isCorridor;
    private GameObject wallOrDoor;

    public void Generate()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (i == 0 || i == width - 1 || j == 0 || j == height - 1)
                {
                    Instantiate<GameObject>(tileBorder, new Vector3(i * 5, 0, j * 5), Quaternion.identity, gameObject.transform);
                    wallOrDoor = wall;
                    foreach(InstantiateDoor instantiateDoor in doors)
                    {
                        if(i== instantiateDoor.positionX && j == instantiateDoor.positionZ)
                        {
                            wallOrDoor = door;
                        }
                        else
                        {
                            wallOrDoor = wall;
                        }
                    }
                    if (!isCorridor)
                    {
                        if (i == 0)
                        {
                            Instantiate<GameObject>(wallOrDoor, new Vector3(-2.5f, 0, (j * 5) + 2.5f), Quaternion.Euler(0, 90, 0), gameObject.transform);
                        }
                        if (j == 0)
                        {
                            Instantiate<GameObject>(wallOrDoor, new Vector3((i * 5) - 2.5f, 0, 2.5f), Quaternion.Euler(0, 0, 0), gameObject.transform);
                        }
                        if (i == width - 1)
                        {
                            Instantiate<GameObject>(wallOrDoor, new Vector3((i * 5) - 2.5f, 0, (j * 5) + 2.5f), Quaternion.Euler(0, -90, 0), gameObject.transform);
                        }
                        if (j == height - 1)
                        {
                            Instantiate<GameObject>(wallOrDoor, new Vector3((i * 5) - 2.5f, 0, (j * 5) + 2.5f), Quaternion.Euler(0, 180, 0), gameObject.transform);
                        }
                    }
                }
                else
                {
                    Instantiate<GameObject>(tileCenter, new Vector3(i * 5, 0, j * 5), Quaternion.identity, gameObject.transform);
                }
            }
        }
    }

    public void Clear()
    {
        foreach (Transform child in transform)
        {
            DestroyImmediate(child.gameObject);
        }
    }
    
}
