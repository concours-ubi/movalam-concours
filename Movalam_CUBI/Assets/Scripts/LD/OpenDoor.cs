using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    private Animator animator;
    private GameObject[] players;
    public Transform refPosition;
    public float actionRay;

    void Start()
    {
        animator = GetComponent<Animator>();
        players = GameObject.FindGameObjectsWithTag("Player");
    }

    private void Update()
    {
        animator.SetBool("Open", checkPlayersTransform());
    }
   bool checkPlayersTransform()
    {
        foreach (GameObject player in players)
        {
            //Debug.Log(Vector3.Distance(player.transform.position, refPosition.position));
            if (Vector3.Distance(player.transform.position, refPosition.position) < actionRay)
            {
                return true;
            }
        }
        return false;
    }
}
