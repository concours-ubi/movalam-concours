using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndPositionController : MonoBehaviour
{

    /// <summary>
    /// Image de l'agent (UI)
    /// </summary>
    [SerializeField] private GameObject UI;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Activate()
    {
        this.UI.SetActive(true);
    }

    public void Deactivate()
    {
        this.UI.SetActive(false);
    }
}
