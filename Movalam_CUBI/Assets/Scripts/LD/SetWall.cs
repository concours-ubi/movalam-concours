using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetWall : MonoBehaviour
{
    private void Awake()
    {
        //if the wall is in the correct rotation, it can desappear to see the player
        if ((transform.rotation.eulerAngles.y / 90) % 2 > -0.1 && (transform.rotation.eulerAngles.y / 90) % 2 < 0.1)
        {
            gameObject.AddComponent<MurTransparent>();
        }
    }
}
