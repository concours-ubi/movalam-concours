using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorTerminal : GenericTerminal
{
    [Header("Detector specificities")]
    public GameObject detectorToDeactivate;                     // Le detecteur � desactiver

    public delegate void DesactivateDetectorEvent();
    public DesactivateDetectorEvent OnDesactivateDetectorEvent;
    public AK.Wwise.Event wwiseEventUnactiveLD;

    //pour le outline
    private Outline setCouleur;
    private Outline setCorespondance;
    private int CodeCouleur;
    private Color CouleurChoisi;

    private void Start()
    {
        setCouleur = gameObject.GetComponentInChildren<Outline>();
        CodeCouleur = DeterminerCouleur.Instance.NombreCouleur();
        CouleurChoisi = DeterminerCouleur.Instance.ChoisirCouleur(CodeCouleur);
        setCouleur.OutlineCouleur(CouleurChoisi);

        setCorespondance = detectorToDeactivate.gameObject.GetComponentInChildren<Outline>();
        setCorespondance.OutlineCouleur(CouleurChoisi);

    }

    public override void Interaction()
    {
        if (!crypted)
        {
            DeactivateDetector();
        }

        base.Interaction();
    }

    public override void Decrypt()
    {
        base.Decrypt();
        DeactivateDetector();
        interactionWindowAnimator.SetBool("first", true);
        interactionWindowAnimator.SetBool("starting", true);
    }

    public void DeactivateDetector()
    {
        OnDesactivateDetectorEvent?.Invoke();
        wwiseEventUnactiveLD.Post(detectorToDeactivate);
        detectorToDeactivate.SetActive(false);
        //EndInteraction();
    }

    public override void EndInteraction()
    {
        base.EndInteraction();
        if (!crypted)
            interactionWindowAnimator.SetBool("first", false);
    }
}
