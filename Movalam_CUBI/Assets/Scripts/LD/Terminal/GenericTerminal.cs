using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GenericTerminal : MonoBehaviour
{
    public bool doUpdate = true;
    public bool triggerActive = false;                                      // Un bool�en qui permet de savoir si un joueur est dans la range
    public bool isInteracting = false;
    public MovementController playerMouvement;

    [Header("Interaction attribute")]
    [SerializeField] private string interactionText;                        // Le texte affich� sur la petite interface de la borne
    [SerializeField] private Canvas interactionWindow;                      // Une r�f�rence sur le canvas correspondant � la fen�tre d'interaction
    public TextMeshProUGUI interactionMessage;
    public GameObject interactionAgentPanel;
    public Animator interactionWindowAnimator;

    [Header("Crypted attribute")]
    [SerializeField] private Canvas cryptedWindow;                          // Une r�f�rence sur le canvas correspondant � la fen�tre crypt�e
    [SerializeField] public bool crypted = true;                            // Indique si la borne doit �tre crypt�e avant
    [SerializeField] private string cryptedText;                            // Le texte affich� sur la petite interface si la borne est crypt�e
    [SerializeField] private Texture2D newCursor;                           // Le curseur pendant le mini jeu

    [Header("Mini game")]
    [SerializeField] private ButtonCombinationController button1;           // Une r�f�rence sur le premier bouton du mini jeu
    [SerializeField] private ButtonCombinationController button2;           // Une r�f�rence sur le deuxi�me bouton du mini jeu
    [SerializeField] private ButtonCombinationController button3;           // Une r�f�rence sur le troisi�me bouton du mini jeu
    [SerializeField] private Animator cryptedAnimator;                      // Une r�f�rence sur l'animator de la fen�tre du mini jeu


    public delegate void DecryptEvent();
    public DecryptEvent OnDecryptEvent;
    public delegate void EndInteractionEvent();
    public EndInteractionEvent OnEndInteractionEvent;
    public AK.Wwise.Event wwiseEventSuccesGame;
    public AK.Wwise.Event wwiseEventFailGame;
    public AK.Wwise.State gameplayState;
    public AK.Wwise.State borneState;


    public virtual void Update()
    {
        if (!doUpdate) return;
        if (triggerActive && !isInteracting && Input.GetKeyDown(KeyCode.E))
        {
            if (!playerMouvement.GetInteracting() && !playerMouvement.GetPaused())
                Interaction();
        }
        else if (Input.GetKeyDown(KeyCode.E) && isInteracting)
        {
            EndInteraction();
        }
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (!doUpdate) return;
        if (other.CompareTag("Player"))
        {
            playerMouvement = other.gameObject.GetComponent<MovementController>();
            interactionAgentPanel = other.gameObject.GetComponent<InteractionController>().interactionPanel;
            interactionMessage = other.gameObject.GetComponent<InteractionController>().interactionText;
            ShowInteractionMessage();
            triggerActive = true;
        }
    }

    public virtual void OnTriggerExit(Collider other)
    {
        if (!doUpdate) return;
        if (other.CompareTag("Player"))
        {
            HideInteractionMessage();
            triggerActive = false;
            if (isInteracting)
                EndInteraction();
            playerMouvement = null;
            interactionAgentPanel = null;
            interactionMessage = null;
        }
    }

    virtual public void Interaction()
    {
        borneState.SetValue();
        //Debug.Log("Borne");
        if(playerMouvement)
        {
            playerMouvement.CantPause();
            playerMouvement.Interacts();
        }

        HideInteractionMessage();
        isInteracting = true;

        if (crypted)
        {
            cryptedWindow.enabled = true;
            cryptedAnimator.SetBool("starting", true);
        }
        else
        {
            interactionWindow.enabled = true;
            interactionWindowAnimator.SetBool("starting", true);
        }
        Vector2 cursorOffset = new Vector2(newCursor.width / 2, newCursor.height / 2);
        Cursor.SetCursor(newCursor, cursorOffset, CursorMode.Auto);
    }

    virtual public void EndInteraction()
    {
        gameplayState.SetValue();
        //Debug.Log("Gameplay");
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

        if (crypted)
        {
            cryptedAnimator.SetBool("starting", false);
        }
        else
        {
            interactionWindowAnimator.SetBool("starting", false);
        }

        if (playerMouvement)
        {
            playerMouvement.CanPause();
            playerMouvement.StopInteracting();
        }

        ShowInteractionMessage();

        if (crypted)
        {
            cryptedWindow.enabled = false;
        }
        else
            interactionWindow.enabled = false;

        isInteracting = false;
        OnEndInteractionEvent?.Invoke();
    }

    virtual public void Decrypt()
    {
        //EndInteraction();
        //ShowInteractionMessage();
        crypted = false;
        OnDecryptEvent?.Invoke();
        cryptedWindow.enabled = false;
        interactionWindow.enabled = true;
    }

    public void ShowInteractionMessage()
    {
        interactionAgentPanel.SetActive(true);
        interactionMessage.text = crypted ? cryptedText : interactionText;
    }

    public void HideInteractionMessage()
    {
        interactionAgentPanel.SetActive(false);
        interactionMessage.text = "";
    }
    public bool isMatched()
    {
        return (button1.GetIsMatched() && button2.GetIsMatched() && button3.GetIsMatched());
    }

    public void SetInteractionMessage(string message)
    {
        interactionMessage.text = message;
    }

    public void TryDecrypt()
    {
        if (isMatched())
        {
            wwiseEventSuccesGame.Post(gameObject);
            Decrypt();
        }
        else
        {
            wwiseEventFailGame.Post(gameObject);
        }
    }
}
