using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MovementDetectorController : MonoBehaviour
{
    [SerializeField] private float speed;                            // La vitesse du detecteur
    [SerializeField] private bool move;                              // Indique si le detecteur doit bouger ou non

    [SerializeField] private Transform maxPos;                       // La position de son fils maxPosition    
    [SerializeField] private Transform minPos;                       // La position de son fils minPosition

    private Vector3 target;                                          // L'endroit vers lequel se dirige le detecteur (min ou max pos)
    private Vector3 nonTarget;                                       // l'autre position que target
    private float distanceBetweenTargets;                            // Distance entre les deux points de la caméra

    private MovementController checkPlayer = null;                   // Le script du joueur se trouvant sur le d�tecteur pour l'emp�cher de se d�placer en mm temps

    public AK.Wwise.Event wwiseEventLaser;

    void Start()
    {
        distanceBetweenTargets = Vector3.Distance(maxPos.position, minPos.position);
        wwiseEventLaser.Post(gameObject);
    }

    void Update()
    {
        if (move)
        {
            // Distance parcourue par la caméra depuis le début
            float distanceToCatch = Time.time;
            if (CustomNetworkManager.singleton)
                distanceToCatch = (float)NetworkTime.time * speed;

            // Nombre de fois qu'elle a changé de target
            int roundTrip = Mathf.FloorToInt(distanceToCatch / distanceBetweenTargets);

            // On choisit la target
            if (roundTrip % 2 == 0)
            {
                target = maxPos.position;
                nonTarget = minPos.position;
            }
            else
            {
                target = minPos.position;
                nonTarget = maxPos.position;
            }

            // On sauvegarde les positions des targets
            Vector3 max = maxPos.position;
            Vector3 min = minPos.position;

            // On se place sur la position où la caméra est censé être entre les deux targets
            transform.position = nonTarget + (target - nonTarget).normalized * (distanceToCatch - roundTrip * distanceBetweenTargets);

            // On restaure les positions des targets
            maxPos.position = max;
            minPos.position = min;
        }

        if (checkPlayer != null)
        {
            if (checkPlayer.GetMoveDirection().x != 0 || checkPlayer.GetMoveDirection().z != 0)
                checkPlayer.gameObject.GetComponentInChildren<DetectionController>().Detected();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Si c'est le joueur, on v�rifie si il est immobile
            Vector3 zero = new Vector3(0, 0, 0);

            checkPlayer = other.gameObject.GetComponent<MovementController>();
            if (other.gameObject.GetComponent<MovementController>().GetMoveDirection().x != 0 || other.gameObject.GetComponent<MovementController>().GetMoveDirection().z != 0)
            {
                other.gameObject.GetComponentInChildren<DetectionController>().Detected();
                checkPlayer = null;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            checkPlayer = null;
        }
    }

    private void OnDisable()
    {
        wwiseEventLaser.Stop(gameObject);
    }
}
