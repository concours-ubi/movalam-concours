using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RoomMaker))]
public class RoomMakerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        RoomMaker roomMaker = (RoomMaker)target;
        if (GUILayout.Button("Generate"))
        {
            roomMaker.Generate();
        }
        if (GUILayout.Button("Clear"))
        {
            roomMaker.Clear();
        }
    }
}
