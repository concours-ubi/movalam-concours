using Steamworks;
using UnityEngine;
using VivoxUnity;
using Steamworks.Data;

public class VivoxManager : MonoBehaviour
{
    private static VivoxManager instance;
    public static VivoxManager Instance
    {
        get
        {
            return instance;
        }
        private
        set
        {
            instance = value;
        }
    }

    VivoxVoiceManager vivox;
    public VivoxVoiceManager Vivox { get { return vivox; } }
    Client client = new Client();

    private void Awake()
    {
        if (instance)
        {
            Debug.Log("VivoxManager is already instanced (destroy duplicate instance)");
            Destroy(gameObject);
            return;
        }
        instance = this;
        vivox = VivoxVoiceManager.Instance;
        client.Uninitialize();
        client.Initialize();

        LobbyManager.OnLobbyEnteredEvent += JoinVoc;
        LobbyManager.OnLobbyLeaveEvent += LeaveVoc;
        vivox.OnUserLoggedInEvent += LoggedIn;
        DontDestroyOnLoad(gameObject);
    }

    private void OnApplicationQuit()
    {
        vivox.Logout();
        client.Uninitialize();
    }

    public void Login(string userName)
    {
        vivox.Login(userName);
    }

    private void LoggedIn()
    {
        JoinChannel(LobbyManager.Instance.currentLobby.Id.ToString() + "_lobby");
    }

    public void Logout()
    {
        if (vivox.LoginState == LoginState.LoggedIn)
        {
            vivox.Logout();
        }
    }

    public void JoinChannel(string channelName)
    {
        vivox.JoinChannel(channelName, ChannelType.NonPositional, VivoxVoiceManager.ChatCapability.AudioOnly);
    }

    public void LeaveChannels()
    {
        vivox.DisconnectAllChannels();
    }

    private void JoinVoc(Lobby lobby)
    {
        Login(SteamClient.Name);
    }

    private void LeaveVoc()
    {
        LeaveChannels();
        Logout();
        Unmute();
    }

    public void Mute()
    {
        client.AudioInputDevices.Muted = true;
    }

    public void Unmute()
    {
        client.AudioInputDevices.Muted = false;
    }
}
