using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PseudoController : MonoBehaviour
{
    public float moveSpeed = 6;

    Rigidbody rb;
    Vector3 velocity;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized * moveSpeed;
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
    }
}
