using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetShadow : MonoBehaviour
{
    MeshRenderer meshRenderer;
    GameObject[] players;
    bool drawShadow;
    void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        meshRenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        drawShadow = CheckViewPlayer();
        if (drawShadow)
        {
            meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        }
        else
        {
            meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }
    }

    bool CheckViewPlayer()
    {
        foreach (GameObject player in players)
        {
            foreach(Transform target in player.GetComponent<FieldOfView>().visibleTargets)
            {
                if(target == gameObject.transform)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
