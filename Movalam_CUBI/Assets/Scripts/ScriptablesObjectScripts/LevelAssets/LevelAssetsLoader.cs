using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class LevelAssetsLoader
{
    private static Dictionary<string, LevelAsset> levelAssets;
    public static Dictionary<string, LevelAsset> GetLevelAssets()
    {
        if (!levelAlreadyLoaded)
            LoadLevelAssets();
        return levelAssets;
    }
    private static bool levelAlreadyLoaded = false;
    public static LevelAsset GetLevelAsset(string levelAssetName)
    {
        if (!levelAlreadyLoaded)
            LoadLevelAssets();
        return levelAssets[levelAssetName];
    }

    public static void LoadLevelAssets()
    {
        levelAssets = new Dictionary<string, LevelAsset>();
        LevelAsset[] levelAssetsArray = Resources.LoadAll<LevelAsset>("LevelAssets");
        foreach (LevelAsset levelAsset in levelAssetsArray)
        {
#if !UNITY_EDITOR
            if (levelAsset.IsDevLevel) continue;
#endif
            levelAssets.Add(levelAsset.name, levelAsset);
        }
        levelAlreadyLoaded = true;
    }
}
