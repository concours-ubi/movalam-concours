using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Difficulty
{
    public enum Difficulties
    {
        Easy,
        Intermediate,
        Hard
    }
    public static Color[] DifficultiesColor =  {
        Color.green,
        Color.yellow,
        Color.red
    };
}
