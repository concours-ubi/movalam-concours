using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour
{
    [SerializeField] private Canvas pauseMenu;     // R�f�rence sur le script du canvas de pause
    [SerializeField] private bool assistant = false;

    private bool open = false;

    private void Start()
    {
        pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu").GetComponent<Canvas>();
    }
    private void Update()
    {
        if (pauseMenu != null)
        {
            if (!assistant)
            {
                if (Input.GetKeyDown(KeyCode.Escape) && open == false)
                {
                    if (!gameObject.GetComponent<MovementController>().GetInteracting() && gameObject.GetComponent<MovementController>().GetCanPause())
                    {
                        open = true;
                        pauseMenu.enabled = true;
                        pauseMenu.GetComponent<PauseMenuController>().Pause(gameObject.GetComponent<MovementController>(), null);
                    }
                }
                else if (Input.GetKeyDown(KeyCode.Escape) && open == true)
                {
                    pauseMenu.GetComponent<PauseMenuController>().Resume();
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Escape) && open == false)
                {
                    open = true;
                    pauseMenu.enabled = true;
                    pauseMenu.GetComponent<PauseMenuController>().Pause(null, gameObject.GetComponentInChildren<AssistantCameraController>());
                }
                else if (Input.GetKeyDown(KeyCode.Escape) && open == true)
                {
                    pauseMenu.GetComponent<PauseMenuController>().Resume();
                }
            }
        }
        else
            Debug.Log("pas de canvas de pos r�f�renc�");
    }

    public void IsNotOpen()
    {
        open = false;
    }
}
