using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AgentPositionController : MonoBehaviour
{
    /// <summary>
    /// Baniere de l'agent 1
    /// </summary>
    [SerializeField] private Sprite banner1;

    /// <summary>
    /// Baniere de l'agent 2
    /// </summary>
    [SerializeField] private Sprite banner2;

    /// <summary>
    /// Baniere de l'agent 3
    /// </summary>
    [SerializeField] private Sprite banner3;

    /// <summary>
    /// Image de l'agent (UI)
    /// </summary>
    [SerializeField] private Image AgentBanner;

    /// <summary>
    /// Image de l'agent (UI)
    /// </summary>
    [SerializeField] private GameObject UI;

    public RectTransform textRecTransform;
    public Vector3 minScale;
    public Vector3 maxScale;
    private AssistantCameraController assistantCameraController;
    private float normalization;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (NetworkAssistant.assistant == null) return;
        else if (assistantCameraController == null)
        {
            assistantCameraController = NetworkAssistant.assistant.asssitantCameraController;
        }
        normalization = (assistantCameraController.transform.position.y - assistantCameraController.MinHeight) / (assistantCameraController.MaxHeight - assistantCameraController.MinHeight);
        textRecTransform.localScale = Vector3.Lerp(minScale, maxScale, normalization);
    }

    public void Activate()
    {
        this.UI.SetActive(true);
    }

    public void Deactivate()
    {
        this.UI.SetActive(false);
    }

    public void SetAgentBySlot(int slotindex)
    {
        switch (slotindex)
        {
            case 1:
                AgentBanner.sprite = banner1;
                break;
            case 2:
                AgentBanner.sprite = banner2;
                break;
            case 3:
                AgentBanner.sprite = banner3;
                break;
            default:
                AgentBanner.sprite = banner1;
                break;
        }
    }
}
