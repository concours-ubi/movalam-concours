using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InteractionController : MonoBehaviour
{
    public GameObject interactionPanel;                    // Une reference sur le panel d'interaction
    public TextMeshProUGUI interactionText;                        // Son texte d'interaction actuel

    public void Start()
    {
        interactionPanel.SetActive(false);
    }
}
