using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonCombinationController : MonoBehaviour
{
    private int actualNumber;                                   // Le chiffre actuel
    private List<string> alphabet = null;

    private string letter;                                      // la lettre sur la carte
    private bool isMatched = false;                             // Indique si la lettre et le chiffre sont un match, true si oui 

    private Text text;

    void Start()
    {
        text = GetComponent<Text>();
        actualNumber = Random.Range(0, 9);
        text.text = actualNumber.ToString();
        ChangeNumber();
    }

    public void ChangeNumber()
    {
        actualNumber = (actualNumber + 1) % 10;
        text.text = actualNumber.ToString();

        // Si ça match, on met à true
        if (alphabet != null)
        {
            if (alphabet.IndexOf(letter) == actualNumber)
            {
                isMatched = true;
            }
            else
                isMatched = false;
        }
    }

    public void SetLetter(string letterSet)
    {
        letter = letterSet;
    }

    public bool GetIsMatched()
    {
        return isMatched;
    }

    public void SetAlphabet(List<string> setAlpha)
    {
        alphabet = setAlpha;
        if (alphabet.IndexOf(letter) == actualNumber)
        {
            isMatched = true;
        }
    }
}
