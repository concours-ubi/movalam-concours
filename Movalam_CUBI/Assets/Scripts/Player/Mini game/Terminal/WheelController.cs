using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WheelController : MonoBehaviour
{
    private List<string> alphabet;

    [SerializeField] private Text[] infoTexts;

    public void Initialise(List<string> randomAlpha)
    {
        alphabet = randomAlpha;

        for (int i = 0; i < infoTexts.Length; i++)
        {
            infoTexts[i].text = alphabet[i];
        }
        // Ensuite ici on initialise le truc d'aide pas encore fait mdr
    }
}
