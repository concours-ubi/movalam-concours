using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombinaisonPanelController : MonoBehaviour
{
    private List<string> alphabet;

    [SerializeField] private RandomLetter randomLet1;
    [SerializeField] private RandomLetter randomLet2;
    [SerializeField] private RandomLetter randomLet3;

    public void Initialise(List<string> randomAlpha)
    {
        alphabet = randomAlpha;
        randomLet1.Initialize(alphabet);
        randomLet2.Initialize(alphabet);
        randomLet3.Initialize(alphabet);
        randomLet3.refreshTakenLetter();
    }
}
