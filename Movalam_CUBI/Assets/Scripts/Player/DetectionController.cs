using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class DetectionController : MonoBehaviour
{
    
    [SerializeField] private float startDetectionCooldown;                 // La dur�e sans envoyer d'emote pour que la barre commence � decrease
    private float detectionCooldown;                                       // Le timer qui varie

    /// <summary>
    ///     Temps en second avant que l'on sois détecté
    /// </summary>
    public float timeForFullToEmpty {
        get
        {
            if (LevelRuleSet.Instance)
                return LevelRuleSet.Instance.levelRuleSet.detectionTime;
            else
                return 5;
        }
    }                  // 

    public float detectionValue;                                           // Valeur de d�tection
    public Image detectionBar;                                             // Le slider de la barre de detection
    private MovementController playerMovement;                             // Une r�f�rence sur le script de d�placement du joueur pour l'emp�cher de bouger quand il est d�tect�

    private bool detected = false;                                         // Un bool�en qui indique si le joueur est d�tect� ou non

    public delegate void DetectedEvent();
    public DetectedEvent OnDetectedEvent;

    public AK.Wwise.Event wwiseEventDetection;
    public AK.Wwise.Event wwiseEventDetectionPorte;
    public AK.Wwise.Event wwiseEventDetected;
    private bool soundIsPlay = false;
    private bool detectionIsAdd = false;
    private int countDetctionNotAdd = 0;

    private void Awake()
    {
        detectionBar.fillAmount = 0;
        playerMovement = GetComponentInParent<MovementController>();
    }
    void Start()
    {
        detectionCooldown = startDetectionCooldown;
    }

    void Update()
    {
        if (!detectionIsAdd)
        {
            countDetctionNotAdd++;
        }
        else
        {
            if (!soundIsPlay)
            {
                soundIsPlay = true;
                wwiseEventDetection.Post(gameObject);
            }
        }
        if (countDetctionNotAdd > 10)
        {
            wwiseEventDetection.Stop(gameObject);
            soundIsPlay = false;
            countDetctionNotAdd = 0;
        }
        detectionIsAdd = false;

        
        if (!detected)
        {
            if (detectionCooldown < startDetectionCooldown)   // Gestion du cooldown
            {
                detectionCooldown -= Time.deltaTime;
                if (detectionCooldown <= 0)
                {
                    detectionCooldown = startDetectionCooldown;
                }
            }
            else if (detectionCooldown == startDetectionCooldown && detectionValue > 0 && !detected)    // Si le cooldown emp�chant la barre de se vider est fini, alors elle se vide
            {
                if (detectionValue >= Time.deltaTime)
                    detectionValue -= Time.deltaTime;
                else
                    detectionValue = 0;
            }
            detectionBar.fillAmount = detectionValue / timeForFullToEmpty;
        }
    }

    /// <summary>
    /// Fonction à appeler pour monter la d�tection, percentage est le pourcentage de la barre qu'on veut remplir
    /// </summary>
    /// <param name="percentage">Le pourcentage a donner</param>
    /// <param name="overtime">Si c'est une valeur par seconde overtime doit être à true</param>
    public void AddDetection(float percentage, bool overtime = false, bool isDoor = false)
    {
        if (!detected)
        {
            if (!isDoor)
            {
                countDetctionNotAdd = 0;
                detectionIsAdd = true;
            }
            else
            {
                wwiseEventDetectionPorte.Post(gameObject);
            }
            float valueToAdd;
            if (overtime)
                valueToAdd = timeForFullToEmpty * percentage * Time.deltaTime;
            else
                valueToAdd = percentage * timeForFullToEmpty;
            if (!enabled) return;
            if ((detectionValue + valueToAdd) >= timeForFullToEmpty)
            {
                detectionValue = timeForFullToEmpty;
                Detected();
            }
            else
            {
                detectionValue += valueToAdd;
                detectionCooldown = startDetectionCooldown;
                detectionCooldown -= Time.deltaTime;
            }
        }
    }

    public void Detected()
    {
        if (!enabled) return;
        if (detected) return;

        OnDetectedEvent?.Invoke();
        detectionValue = timeForFullToEmpty;
        detectionBar.fillAmount = detectionValue / timeForFullToEmpty;
        detected = true;
        wwiseEventDetection.Stop(gameObject);
        wwiseEventDetected.Post(gameObject);
        playerMovement.Detected();

        // Respawn();
        // Dire a l'assistant que le joueur est au sol (faire appara�tre la barre et commencer le d�compte 
        // sachant qu'il faut faire (startTime - downCounter * decreaseTimePerDown) avec un min de minTime, toutes ces variables sont dans NetworkAssistant) 
        // s'il est dessus faut lancer le mini jeu, qui devra appeler respawn ici apr�s r�solution (pour l'instant j'appelle respawn direct)
    }

    public void Respawn()
    {
        playerMovement.Respawn();
        detectionCooldown = startDetectionCooldown;
        detectionValue = 0;
        detected = false;
    }

    public bool IsAgentDetected()
    {
        return detected;
    }
}
