using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [SerializeField] private float speed = 6f;                                  // La vitesse du joueur
    [SerializeField] private float gravity = 20f;                               // La gravit� qui affecte le joueur

    private float mx;                                                           // Input en x du joueur
    private float mz;                                                           // Input en z du joueur
    private Vector3 moveDirection = Vector3.zero;                               // Variable permettant de stocker le vecteur de mouvement du joueur
    private CharacterController characterController;                            // Une r�f�rence sur le characterController du joueur
    private bool detected = false;                                              // True si le joueur est detecte, false sinon
    private bool interacting = false;                                           // Indique si le joueur est en train d'interagir (pour l'animation)
    private bool paused = false;                                                // Indique si le menu de pause est ouvert
    private bool canPause = true;

    private Vector3 respawnPosition = Vector3.zero;                             // La position de respawn du joueur (change � chaque fois qu'il touche un checkpoint)
    public Animator animator;                                                  // Une r�f�rence sur l'animator du joueur

    public GameObject[] charactersSprites;

    private void Awake()
    {
        Cursor.visible = true;
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (!detected && !interacting && !paused) // Si le joueur n'est pas detect�, il peut bouger
        {
            if (characterController.isGrounded)
            {
                mx = Input.GetAxisRaw("Horizontal");
                mz = Input.GetAxisRaw("Vertical");
                moveDirection = new Vector3(mx, 0, mz).normalized;
                moveDirection = transform.TransformDirection(moveDirection);
                if (mx == 0 && mz == 0)
                {
                    animator.SetFloat("Magnitude", 0);
                }
                else
                {
                    animator.SetFloat("Horizontal", moveDirection.x);
                    animator.SetFloat("Vertical", moveDirection.z);
                    animator.SetFloat("Magnitude", moveDirection.magnitude);
                }
            }
            moveDirection.y -= gravity * Time.deltaTime;

            characterController.Move(moveDirection * Time.deltaTime * speed);
        }
    }

    private void LateUpdate()
    {
        if (!interacting)
            canPause = true;
    }

    public void Detected()
    {
        detected = true;
        interacting = false;
        animator.SetBool("Down", detected);
        animator.SetBool("Interaction", interacting);
    }

    public Vector3 GetMoveDirection()
    {
        return moveDirection;
    }

    public void Respawn()
    {
        detected = false;
        characterController.enabled = false;
        characterController.transform.position = respawnPosition;
        characterController.enabled = true;
        animator.SetBool("Down", detected);
        animator.SetBool("Interaction", interacting);
    }

    public void SetAgentCharacter(int slot)
    {
        animator.gameObject.SetActive(false);
        charactersSprites[slot].gameObject.SetActive(true);
        animator = charactersSprites[slot].GetComponent<Animator>();
    }

    public void SetCheckpoint(Vector3 resPosition)
    {
        respawnPosition = resPosition;
    }

    public void Interacts()
    {
        interacting = true;
        animator.SetBool("Interaction", interacting);
    }

    public void StopInteracting()
    {
        interacting = false;
        animator.SetBool("Interaction", interacting);
    }

    public bool GetInteracting()
    {
        return interacting;
    }

    public void Pause()
    {
        paused = true;
    }

    public void Resume()
    {
        paused = false;
    }

    public bool GetPaused()
    {
        return paused;
    }

    public void CantPause()
    {
        canPause = false;
    }

    public void CanPause()
    {
        canPause = true;
    }

    public bool GetCanPause()
    {
        return canPause;
    }
}
