using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeterminerCouleur : MonoBehaviour
{
   public static DeterminerCouleur Instance { get; private set; }

    public int Value = 0 ;

    public Color couleur = Color.red;

    Color[] listeCouleur = {  
                             new Color(1, 0.92f, 0), // Jaune
                             new Color(0.94f, 0.58f, 0), // Orange
                             new Color(0.88f, 0 , 0.09f), // Rouge
                             new Color(0, 0.658f, 0.552f), // Turquoise fonc�
                             new Color(1, 0.04f, 0.82f), // Rose
                             new Color(0.384f, 0, 0.701f), // Violet fonc�
                             new Color(0.062f, 0.490f, 0.027f), // Vert fonc�
                             new Color(0.478f, 0.188f, 0), // Marron fonc�
                             new Color(1, 0.721f, 0.541f), // Beige
                             new Color(0, 0, 1), // Bleu fonc�
                             new Color(0.494f, 0.827f, 0.984f), // Bleu clair
                             new Color(0.301f, 0.572f, 0.674f), // Bleu Gris
                             new Color(0.427f, 0.027f, 0.101f), // Bordeaux
                             new Color(0.729f, 0.380f, 1), // Violet clair
                             new Color(0.192f, 1, 0.160f), // Vert qui pique
                             new Color(0.760f, 0.670f, 0.4f) // Couleur chelou
    };

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(gameObject); 
        }
    }

    public int NombreCouleur()
    {

       if (Value < (listeCouleur.Length - 1) )
       {
           Value++;
       }
       else
       {
           Value = 0;
       }

        return Value;
    }
    public Color ChoisirCouleur(int CouleurValue)
    {
        

        couleur = listeCouleur[CouleurValue];


        return couleur;
    }
    
}
