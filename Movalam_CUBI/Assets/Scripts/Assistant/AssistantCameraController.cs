using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

// TODO Remplacer l'acceleration par une Curve afin d'eviter qu'elle soit lineaire

public class AssistantCameraController : MonoBehaviour
{
    /// <summary>
    /// L'assistant auquel est attachée la caméra
    /// </summary>
    [SerializeField] private LocalAssistant Assistant;

    /// <summary>
    /// L'assistant network auquel est attachée la caméra, utilisé pour la prise de focus sur le joueur avec les raccourcis clavier
    /// </summary>
    [SerializeField] private NetworkAssistant AssistantNetWork;

    /// <summary>
    /// Vitesse de mouvement de la camera
    /// </summary>
    [SerializeField] private float PanningSpeed = 20f;

    /// <summary>
    /// Coefficient d'acceleration initial de la camera
    /// </summary>
    [SerializeField] private AnimationCurve MovementSpeedup;

    /// <summary>
    /// Taille de la zone d'activation du mouvement a la souris
    /// </summary>
    [SerializeField] private float PanningBorderThickness= 4f;

    /// <summary>
    /// Vitesse de d�filement de la molette
    /// </summary>
    [SerializeField] private float ScrollSpeed = 2f;

    /// <summary>
    /// Hauteur minimale de la caméra asistant
    /// </summary>
    [SerializeField] public int MinHeight = 10;

    /// <summary>
    /// Hauteur maximale de la caméra asistant
    /// </summary>
    [SerializeField] public int MaxHeight = 100;

    /// <summary>
    /// Valeurs de base de l'offest en mode focus
    /// </summary>
    [SerializeField] private Vector3 FocusOffsetBase = new Vector3(0, 5, -5);

    /// <summary>
    /// Coefficient d'acceleration initial de la camera
    /// </summary>
    [SerializeField] private AnimationCurve FocusHeightOffset;

    /// <summary>
    /// Premier point de restriction de la camera
    /// </summary>
    private GameObject TopRightLimit;

    /// <summary>
    /// Deuxieme point de restriction de la camera
    /// </summary>
    private GameObject BottomLeftLimit;
    /// <summary>
    /// Joueur focus par l'assistant
    /// </summary>
    private GameObject Focus;

    /// <summary>
    /// Position de la camera du joueur
    /// </summary>
    private Transform FocusCamera;

    /// <summary>
    /// Coefficient d'acceleration de la camera (les premiers mouvements sont lents afin d'eviter les actions 
    /// non voulues par l'utilisateur quand il cherche les composants de l'interface)
    /// </summary>
    private float AccelerationCoeff = 0f;

    /// <summary>
    /// Indique si l'assistant a ouvert le menu de pause
    /// </summary>
    private bool paused = false;

    /// Moment auquel la cam�ra a commenc� � bouger
    /// </summary>
    private float MoveStart = 0f;

    /// <summary>
    /// Référence sur le script de pause pour indiquer quand le menu est fermé
    /// </summary>
    public PauseController pauseCont;

    /// <summary>
    /// Hauteur minimale de la caméra asistant
    /// </summary>
    private Vector3 FocusOffset ;

    void Awake()
    {
        TopRightLimit = GameObject.Find("TopRightLimit");
        BottomLeftLimit = GameObject.Find("BottomLeftLimit");
        this.gameObject.transform.position = GameObject.Find("MidlleMap").transform.position;
    }

    void Update()
    {
        if (!paused)
        {
            // Gestion de la prise de focus sur les agents
            // Prise de focus par clic sur l'agent dans la scène
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                RaycastHit hit;
                Ray ray = this.GetComponentInChildren<Camera>().ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    // Si le raycast touche un joueur, on initialise le focus
                    if (hit.collider.gameObject.tag == "Player")
                    {
                        SetFocusOn(hit.collider.gameObject);
                    }
                }
            }

            // Prise de focus par les touches 1, 2, 3
            if (AssistantNetWork != null)
            {
                GameObject[] playerFrames = GetPlayerFrames();
                if (Input.GetKey(KeyCode.Alpha1) && playerFrames.Length >= 1)
                {
                    SetFocusOn(playerFrames[0].GetComponent<PlayerFrameController>().GetNetworkIdentity().gameObject);
                }
                if (Input.GetKey(KeyCode.Alpha2) && playerFrames.Length >= 2)
                {
                    SetFocusOn(playerFrames[1].GetComponent<PlayerFrameController>().GetNetworkIdentity().gameObject);
                }
                if (Input.GetKey(KeyCode.Alpha3) && playerFrames.Length >= 3)
                {
                    SetFocusOn(playerFrames[2].GetComponent<PlayerFrameController>().GetNetworkIdentity().gameObject);
                }
            }

            // Prise de focus par un clic sur le cadre d'un joueur
            if (Assistant.IsAPlayerSelected())
            {
                //Debug.Log("Focus pris sur " + Assistant.GetSelectedPlayer().playerText);
                if (Focus != Assistant.GetSelectedPlayer().GetNetworkIdentity().gameObject)
                    SetFocusOn(Assistant.GetSelectedPlayer().GetNetworkIdentity().gameObject);
            }

            if (Focus != null)
            {
                // Gestion du zoom avec la molette, le * 200 est un cofficient statique pour accelerer le mouvement de camera
                float Scroll = Input.GetAxis("Mouse ScrollWheel");
                FocusOffset.y -= Scroll * ScrollSpeed * Time.deltaTime * 200f;

                // Gestion du zoom avec les touches shift et control, le * 10 est un cofficient statique pour accelerer le mouvement de camera
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    FocusOffset.y += ScrollSpeed * Time.deltaTime * 10f;
                }

                if (Input.GetKey(KeyCode.LeftControl))
                {
                    FocusOffset.y -= ScrollSpeed * Time.deltaTime * 10f;
                }

                // Si on a un joueur en focus, on colle la position de la cam�ra assistant � celle de l'agent et on applique l'offest
                FocusCamera = Focus.GetComponentInChildren<Camera>().transform;
                FocusOffset.y = Mathf.Clamp(FocusCamera.position.y + FocusOffset.y, MinHeight, MaxHeight) - FocusCamera.position.y;
                FocusOffset.z = FocusOffsetBase.z - FocusHeightOffset.Evaluate(FocusOffset.y); 
                transform.position = FocusCamera.position + FocusOffset;

                // Si le joueur utilise diff�rents contr�le pour repartir en cam�ra libre ...
                if (Input.mousePosition.y >= Screen.height - PanningBorderThickness ||
                    Input.mousePosition.y <= PanningBorderThickness ||
                    Input.mousePosition.x <= PanningBorderThickness ||
                    Input.mousePosition.x >= Screen.width - PanningBorderThickness ||
                    Input.GetKey(KeyCode.W) ||
                    Input.GetKey(KeyCode.A) ||
                    Input.GetKey(KeyCode.S) ||
                    Input.GetKey(KeyCode.D) ||
                    Input.GetKey(KeyCode.UpArrow) ||
                    Input.GetKey(KeyCode.DownArrow) ||
                    Input.GetKey(KeyCode.LeftArrow) ||
                    Input.GetKey(KeyCode.RightArrow))
                {
                    //  ... On annule le focus
                    Focus = null;
                    FocusCamera = null;
                    if (Assistant.IsAPlayerSelected())
                    {
                        Assistant.GetSelectedPlayer().RemoveFocus();
                        Assistant.SetSelectedPlayer(null);
                    }
                    FocusOffset = FocusOffsetBase;
                }
            }
            else
            {
                if ((Input.mousePosition.y >= Screen.height - PanningBorderThickness ||
                    Input.mousePosition.y <= PanningBorderThickness ||
                    Input.mousePosition.x <= PanningBorderThickness ||
                    Input.mousePosition.x >= Screen.width - PanningBorderThickness ||
                    Input.GetKey(KeyCode.UpArrow) ||
                    Input.GetKey(KeyCode.DownArrow) ||
                    Input.GetKey(KeyCode.RightArrow) ||
                    Input.GetKey(KeyCode.LeftArrow)) ||
                    Input.GetKey(KeyCode.W) ||
                    Input.GetKey(KeyCode.A) ||
                    Input.GetKey(KeyCode.S) ||
                    Input.GetKey(KeyCode.D) &&
                    MoveStart == 0f)
                {
                    MoveStart = Time.time;
                }

                // Gestion de l'acceleration de la camera via une AnimationCurve
                AccelerationCoeff = MovementSpeedup.Evaluate(Time.time - MoveStart);

                Vector3 pos = transform.position;

                // Gestion des contr�les clavier et souris, on applique a chaque mouvement le coefficient d'acceleration 
                // ET UN COEFFICIENT POUR CONTRER L'EFFET RALENTISSEMENT EN HAUTEUR 
                if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - PanningBorderThickness)
                {
                    pos.z += PanningSpeed * Time.deltaTime * AccelerationCoeff * pos.y / 16;
                }
                if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || Input.mousePosition.y <= PanningBorderThickness)
                {
                    pos.z -= PanningSpeed * Time.deltaTime * AccelerationCoeff * pos.y / 16;
                }
                if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || Input.mousePosition.x <= PanningBorderThickness)
                {
                    pos.x -= PanningSpeed * Time.deltaTime * AccelerationCoeff * pos.y / 16;
                }
                if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - PanningBorderThickness)
                {
                    pos.x += PanningSpeed * Time.deltaTime * AccelerationCoeff * pos.y / 16;
                }

                // Gestion du zoom avec la molette, on n'applique pas le coefficient d'acceleration pour le zoom, le * 200 est un cofficient 
                // statique pour accelerer le mouvement de camera
                float Scroll = Input.GetAxis("Mouse ScrollWheel");
                pos.y -= Scroll * ScrollSpeed * Time.deltaTime * 200f;

                if (Input.GetKey(KeyCode.LeftShift)) {
                    pos.y += ScrollSpeed * Time.deltaTime * 10f;
                }

                if (Input.GetKey(KeyCode.LeftControl))
                {
                    pos.y -= ScrollSpeed * Time.deltaTime * 10f;
                }

                // Restrictions de la position camera en fonction des points de limite
                pos.x = Mathf.Clamp(pos.x, BottomLeftLimit.transform.position.x, TopRightLimit.transform.position.x);
                pos.z = Mathf.Clamp(pos.z, BottomLeftLimit.transform.position.z, TopRightLimit.transform.position.z);
                pos.y = Mathf.Clamp(pos.y, MinHeight, MaxHeight);

                if (transform.position == pos)
                {
                    MoveStart = 0f;
                }

                // On finit par d�placer la cam�ra
                transform.position = pos;
            }
        }
    }

    private void SetFocusOn(GameObject focus)
    {
        // On enlève l'ancian focus
        if (Assistant.IsAPlayerSelected())
        {
            Assistant.GetSelectedPlayer().RemoveFocus();
            Assistant.SetSelectedPlayer(null);
        }

        Focus = focus;
        FocusCamera = focus.GetComponentInChildren<Camera>().transform;
        FocusOffset = FocusOffsetBase;

        GameObject[] playerFrames = GetPlayerFrames();
        foreach (GameObject frame in playerFrames)
        {
            if (frame.GetComponent<PlayerFrameController>().GetNetworkIdentity().gameObject == focus)
            {
                frame.GetComponent<PlayerFrameController>().ClickedFrame();
            }
        }
        //Debug.Log("Gained focus on " + Focus + " at pos " + FocusCamera);
    }

    public void Pause()
    {
        paused = true;
    }

    public void Resume()
    {
        paused = false;
    }

    public GameObject[] GetPlayerFrames()
    {
        return GameObject.FindGameObjectsWithTag("PlayerFrame");
    }
}
