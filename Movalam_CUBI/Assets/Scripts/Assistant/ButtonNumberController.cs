using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonNumberController : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] public int number;                                         // Le nombre affich� sur le bouton
    private Text text;                                                          // Une r�f�rence sur son fils texte

    private MinigameAssistantController minigameScript;                         // Une r�f�rence sur le script du minijeu
    private Animator animator;                                                  // R�f�rence sur son component animator
    private bool clicked = false;                                               // bool pour emp�cher le multiclick

    void Start()
    {
        animator = GetComponent<Animator>();
        minigameScript = GetComponentInParent<MinigameAssistantController>();
        text = GetComponentInChildren<Text>();
        text.text = number.ToString();
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (clicked == false)
        {
            clicked = true;
            minigameScript.SpawnButton();
            animator.SetBool("destroyed", true);
        }
    }
}
