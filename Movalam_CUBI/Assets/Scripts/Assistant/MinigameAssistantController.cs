using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinigameAssistantController : MonoBehaviour
{
    [SerializeField] private NetworkAssistant assistantController;          // Une r�f�rence sur le script de l'assistant

    [SerializeField] public int numberOfNumbers;                            // Le nombre de bouton �Ecliquer avant la fin du mini jeu
    private int numberLeft;                                                 // Le nombre de bouton restant
    private int numberNext;                                                 // Le prochain nombre �Eutiliser

    [SerializeField] private GameObject buttonNumber;                       // Le GameObject du bouton �Einstancier
    private ButtonNumberController buttonNumberController;                  // Le script du bouton �Einstancier

    private int randomX;                                                    // La position al�atoire en X du bouton �Einstancier
    private int randomY;                                                    // La position al�atoire en Y du bouton �Einstancier
    private Vector3 buttonPosition;                                         // Le vecteur de position al�atoire du bouton �Einstancier

    public Image deathBar;                                                  // R�f�rence sur la barre de mort pour qu'on puisse la modifier
    public Text timeValue;                                                  // R�f�rence sur le texte qui affiche le temps qu'il reste avant de mourir
    public PlayerFrameController actualDedBeingSaved = null;                // R�f�rence sur le script de frame du joueur qui est actuellement en train de se faire sauver

    [SerializeField] private RectTransform topLeft;                         // R�f�rence sur le point en haut �Egauche du cadre
    [SerializeField] private RectTransform bottomRight;                     // R�f�rence sur le point en bas �Edroite du cadre

    [SerializeField] private Transform horizontalCursorBar;                 // R�f�rence sur la barre horizontale du curseur
    [SerializeField] private Transform verticalCursorBar;                   // R�f�rence sur la barre verticale du curseur

    private Animator animator;                                              // R�f�rence sur son animator

    public float startTimeBeforeDead;                                       // Temps qu'il faut pour mourir accessible

    public AK.Wwise.Event wwiseEventSuccess;

    private void OnEnable()
    {
        animator = GetComponent<Animator>();
        buttonNumberController = buttonNumber.GetComponent<ButtonNumberController>();
        numberLeft = numberOfNumbers;
        numberNext = 1;
        Cursor.visible = false;
        //SpawnButton();
    }

    private void Update()
    {
        verticalCursorBar.position = new Vector3(Input.mousePosition.x, verticalCursorBar.position.y, verticalCursorBar.position.z);
        horizontalCursorBar.position = new Vector3(horizontalCursorBar.position.x, Input.mousePosition.y, horizontalCursorBar.position.z);
    }

    public void SpawnButton()
    {
        if (numberLeft > 0)
        {
            // Si on veut qu'ils spawnent sur tout l'�cran
            //randomX = Random.Range(0, Screen.width - (int)(150 * GetComponent<Canvas>().scaleFactor));
            //randomY = Random.Range(0, Screen.height - (int)(150 * GetComponent<Canvas>().scaleFactor));

            // Si on veut qu'ils spawnent dans le cadre, pas s�r du code encore 
            randomX = Random.Range((int)topLeft.position.x, (int)bottomRight.position.x - (int)(150 * GetComponent<Canvas>().scaleFactor));
            randomY = Random.Range((int)bottomRight.position.y, (int)topLeft.position.y - (int)(150 * GetComponent<Canvas>().scaleFactor));

            buttonPosition = new Vector3(randomX, randomY, 0);
            buttonNumberController.number = numberNext;

            Instantiate(buttonNumber, buttonPosition, Quaternion.identity, transform);

            numberLeft -= 1;
            numberNext += 1;
        }
        else
        {
            numberLeft -= 1;
            numberNext += 1;
            animator.Play("EndMinigame");
            assistantController.EndMinigame();
        }
    }

    public void EndMinigame()
    {
        Cursor.visible = true;
        actualDedBeingSaved = null;
        numberLeft = numberOfNumbers;
        numberNext = 1;
        wwiseEventSuccess.Post(gameObject);
        assistantController.BackToGameplay();
    }
}
