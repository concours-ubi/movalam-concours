using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalBarsAnimator : MonoBehaviour
{
    private float height;

    private int speed;
    [SerializeField] private RectTransform cadre;
    private int direction;
    private Vector3 target;
    private Vector2 position;
    private RectTransform rt;

    void Start()
    {
        height = Random.Range(30, 250);
        GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().rect.width, height);
        rt = GetComponent<RectTransform>();
        speed = Random.Range(25, 100);
        direction = Random.Range(0, 2);
        if (direction == 0)
            direction = -1;
    }

    void Update()
    {
        position = rt.anchoredPosition;
        if (direction == 1)
        {
            if ((position.y + speed * Time.deltaTime + height / 2) < cadre.rect.height/2)
                position.y += speed * Time.deltaTime;
            else if ((position.y + speed * Time.deltaTime + height / 2) >= cadre.rect.height/2)
            {
                position.y -= speed * Time.deltaTime;
                direction = -1;
            }
        }
        else if (direction == -1)
        {
            if ((position.y - speed * Time.deltaTime - height / 2) >= -cadre.rect.height/2)
                position.y -= speed * Time.deltaTime;
            else if ((position.y - speed * Time.deltaTime - height / 2) < -cadre.rect.height/2)
            {
                position.y += speed * Time.deltaTime;
                direction = 1;
            }
        }
        rt.anchoredPosition = position;
    }
}
