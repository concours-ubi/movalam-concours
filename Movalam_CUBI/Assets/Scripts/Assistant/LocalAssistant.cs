﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalAssistant : MonoBehaviour
{
    public Transform playerPanel;
    [SerializeField]
    GameObject playerCardPrefab;
    public Canvas minigameCanvas;
    public Canvas gameplayCanvas;

    private PlayerFrameController selectedPlayer = null;                          // Le nom du joueur sélectionné, à terme ce sera l'objet joueur ou une r�f�rence sur son script


    public AK.Wwise.Event wwiseEventSendEmote;


    public delegate void SendEmoteEvent(Emote emoteToSend);
    public SendEmoteEvent OnSendEmoteEvent;

    private void Start()
    {
        Cursor.visible = true;
    }

    public void SetSelectedPlayer(PlayerFrameController player)
    {
        selectedPlayer = player;
    }

    public PlayerFrameController GetSelectedPlayer()
    {
        return selectedPlayer;
    }

    public bool IsAPlayerSelected()
    {
        if (selectedPlayer != null)
            return true;
        else
            return false;
    }

    public PlayerFrameController AddPlayerFrameController(string name, Sprite avatar)
    {
        GameObject card;
        card = Instantiate(playerCardPrefab, playerPanel);
        PlayerFrameController frame = card.GetComponentInChildren<PlayerFrameController>();
        frame.SetPlayerFrame(name, avatar);
        return frame;
    }

    public void SendEmote(Emote emoteToSend)
    {
        wwiseEventSendEmote.Post(gameObject);
        OnSendEmoteEvent?.Invoke(emoteToSend);
    }

}
