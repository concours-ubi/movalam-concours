using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Steamworks;
using UnityEngine.UI;

// Message envoy�Elorsqu'un agent est mis au sol
public struct AgentDownMessage : NetworkMessage
{
    public uint senderId;
    public float detectedTime;
    public float detectionTimeEnd;
}

// Message envoy�Elorsque l'assistant r�ussi le minijeu pour sauver un agent
public struct AgentSavedMessage : NetworkMessage
{
    public uint receiverId;
}

public class NetworkAssistant : NetworkBehaviour
{
    public Camera assistantCamera;
    public PauseController pauseController;
    public Transform canvas;
    public LocalAssistant localAssistant;
    public AssistantCameraController asssitantCameraController;
    public static NetworkAssistant assistant;

    [Header("Minigame Time")]
    public float startTimeForMinigame;                          // Le temps initial pour faire le minijeu
    public float decreaseTimePerDown;                           // Le temps perdu au temps initial par down
    public float minTime;                                       // Le temps minimum pour le minijeu
    private int downCounter = 0;                                // Le compteur de mort qui r�duit le temps disponible pour faire le minijeu
    [SerializeField] private AlgorithmCardController algo;      // R�f�rence sur le script de l'algo pour ajuster la difficult�E(plus on meurt, plus on a d'�mote utile)

    [Header("Canvas")]
    [SerializeField] private Canvas minigameCanvas;             // Le canvas du mini jeu
    [SerializeField] private Canvas gameplayCanvas;             // Le canvas habituel de l'assistant

    [Header("Detection")]
    [SerializeField] private GameObject agentDownAlertText;
    public AK.Wwise.Event playerDownSFX;

    //private ArrayList detectedAgents = new ArrayList();         // Liste des agents d�t�ct�s
    private NetworkIdentity agentSaving;                        // NetworkId de l'agent en train d'�tre sauv�Epar l'assistant

    [SyncVar]
    public ulong steamId;

    public SyncList<NetworkIdentityLink> networkIdentityLinks = new SyncList<NetworkIdentityLink>();
    private List<PlayerFrameController> netIdPlayerFrameMap = new List<PlayerFrameController>();

    private void Awake()
    {
        networkIdentityLinks.Callback += AddFrameController;
        RemoveAllFrame();
    }
    private void OnDestroy()
    {
        SynchroStart.OnGameStarted -= EnableController;
        networkIdentityLinks.Callback -= AddFrameController;
        if (assistant == this)
        {
            assistant = null;
        }
    }

    private void Start()
    {
        if (!isLocalPlayer)
        {
            assistantCamera.gameObject.SetActive(false);
            canvas.gameObject.SetActive(false);
            pauseController.enabled = false;
        }
        else
        {
            assistant = this;
            asssitantCameraController.enabled = false;
            SynchroStart.OnGameStarted += EnableController;
            VivoxManager.Instance.Mute();
            localAssistant.OnSendEmoteEvent += SendEmote;

            NetworkClient.RegisterHandler<AgentSavedMessage>(OnAgentSavedMessage);
            NetworkClient.RegisterHandler<AgentDownMessage>(OnAgentDownMessage);

            GetComponent<EndgameReceiver>().enabled = true;
            GetComponent<GameOverReceiver>().enabled = true;
        }
    }

    public GameObject InitPlayerFrame(NetworkIdentityLink networkIdentityLink)
    {
        SteamProfile agentSteamProfile = LobbyManager.Instance.profiles[networkIdentityLink.steamId];
        PlayerFrameController agentFrame = localAssistant.AddPlayerFrameController(agentSteamProfile.steamName, agentSteamProfile.steamAvatar);
        agentFrame.SetNetworkdIdentity(networkIdentityLink.networkIdentity);
        netIdPlayerFrameMap.Add(agentFrame);
        return agentFrame.transform.parent.gameObject;
    }

    public void AddFrameController(SyncList<NetworkIdentityLink>.Operation op, int itemIndex, NetworkIdentityLink networkIdentityLinkOld, NetworkIdentityLink networkIdentityLinkNew)
    {
        if (!isLocalPlayer) return;
        switch (op)
        {
            case SyncList<NetworkIdentityLink>.Operation.OP_ADD:
                InitPlayerFrame(networkIdentityLinkNew);
                break;
            case SyncList<NetworkIdentityLink>.Operation.OP_CLEAR:
                RemoveAllFrame();
                break;
            case SyncList<NetworkIdentityLink>.Operation.OP_INSERT:
                InitPlayerFrame(networkIdentityLinkNew).transform.SetSiblingIndex(itemIndex);
                break;
            case SyncList<NetworkIdentityLink>.Operation.OP_REMOVEAT:
                PlayerFrameController agentFrame = netIdPlayerFrameMap.Find(frame => frame.GetNetworkIdentity().netId == networkIdentityLinkOld.networkIdentity.netId);
                GameObject playerFrameRoot = agentFrame.transform.parent.gameObject;
                netIdPlayerFrameMap.Remove(agentFrame);
                Destroy(playerFrameRoot);
                break;
            case SyncList<NetworkIdentityLink>.Operation.OP_SET:
                Debug.LogError("Set in AddFrameController");
                break;
            default:
                break;
        }
    }

    public void SendEmote(Emote emoteToSend)
    {
        if (isServer)
        {
            NetworkServer.SendToAll<EmoteID>(new EmoteID { id = emoteToSend.correspondingNumber });
        }
        else
        {
            SendEmoteToServer(new EmoteID { id = emoteToSend.correspondingNumber });
        }
    }

    [Command]
    private void SendEmoteToServer(EmoteID emoteId)
    {
        NetworkServer.SendToAll<EmoteID>(emoteId);
    }

    // M�thode appel�e lorsqu'un agent est mis au sol
    private void OnAgentDownMessage(AgentDownMessage msg)
    {
        // Si il est d�j� sur ce joueur �a lance le minijeu, sinon �a affiche � l'�cran
        //agentDownAlertText.enabled = true;
        playerDownSFX.Post(gameObject);
        agentDownAlertText.SetActive(true);
        minigameCanvas.gameObject.GetComponent<MinigameAssistantController>().startTimeBeforeDead = startTimeForMinigame;
        GetPlayerFrameController(msg.senderId).playerButton.onClick.AddListener(StartMinigame);
        GetPlayerFrameController(msg.senderId).TimeToSaveBar.Detected(msg.detectedTime);
    }

    [Command]
    private void SendAgentSavedMessage(NetworkIdentity agent)
    {
        agent.GetComponent<NetworkAgent>().RespawnAgentOnNetwork();
    }

    private void OnAgentSavedMessage(AgentSavedMessage msg)
    {
        if (!isLocalPlayer) return;
        GetPlayerFrameController(agentSaving.netId).playerButton.onClick.RemoveListener(StartMinigame);
        GetPlayerFrameController(agentSaving.netId).TimeToSaveBar.Saved();
    }


    public void EndMinigame()
    {
        SendAgentSavedMessage(agentSaving);
        GetPlayerFrameController(agentSaving.netId).playerButton.onClick.RemoveListener(StartMinigame);
        GetPlayerFrameController(agentSaving.netId).TimeToSaveBar.Saved();
    }

    public void BackToGameplay()
    {
        minigameCanvas.gameObject.SetActive(false);
        gameplayCanvas.enabled = true;
    }

    public void StartMinigame()
    {
        PlayerFrameController selectedPlayer = localAssistant.GetSelectedPlayer();
        if (selectedPlayer.TimeToSaveBar.TimeOut)
        {
            selectedPlayer.playerButton.onClick.RemoveListener(StartMinigame);
            return;
        }

        agentSaving = selectedPlayer.GetNetworkIdentity();
        //agentDownAlertText.enabled = false;
        agentDownAlertText.SetActive(false);
        minigameCanvas.gameObject.SetActive(true);
        minigameCanvas.gameObject.GetComponent<MinigameAssistantController>().actualDedBeingSaved = selectedPlayer;
        gameplayCanvas.enabled = false;

        if (isServer)
            RpcIncDownCounter();
        else
            CmdIncDownCounter();

        if (startTimeForMinigame > minTime)
        {
            startTimeForMinigame -= decreaseTimePerDown;
        }
        //algo.AdjustDifficulty();


        // Il faut lancer si on clique sur l'image du joueur au sol 
        // La fin du jeu est g�r�e dans EndMinigame() de MinigameAssistantController
    }
    
    public void RemoveAllFrame()
    {
        foreach (Transform child in localAssistant.playerPanel)
        {
            Destroy(child.gameObject);
        }
    }

    [Command]
    void CmdIncDownCounter()
    {
        RpcIncDownCounter();
    }

    [ClientRpc]
    void RpcIncDownCounter()
    {
        downCounter++;
    }

    public int GetDownCounter()
    {
        return downCounter;
    }

    public PlayerFrameController GetPlayerFrameController(uint netId)
    {
        for (int i = 0; i < netIdPlayerFrameMap.Count; i++)
        {
            if (netId == netIdPlayerFrameMap[i].GetNetworkIdentity().netId)
                return netIdPlayerFrameMap[i];
        }
        return null;
    }

    public void EnableController()
    {
        asssitantCameraController.enabled = true;
    }
}
