using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldViewController : MonoBehaviour
{
    /// <summary>
    /// Hauteur � partir de laquelle s'affichent les pastilles
    /// </summary>
    [SerializeField] private int triggerHeight;

    /// <summary>
    /// Hauteur � partir de laquelle s'affichent les pastilles
    /// </summary>
    private bool worldViewEnabled;

    /// <summary>
    /// Liste des controleurs de position dans le monde
    /// </summary>
    private AgentPositionController[] agentPositionControllers;

    /// <summary>
    /// controleur marqueur de fin
    /// </summary>
    private EndPositionController endPositionController;

    void Awake()
    {
       agentPositionControllers = GameObject.FindObjectsOfType<AgentPositionController>();   
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.transform.position.y >= triggerHeight && !worldViewEnabled)
        {
            agentPositionControllers = GameObject.FindObjectsOfType<AgentPositionController>();
            endPositionController = GameObject.FindObjectOfType<EndPositionController>();
            ActivateAll();
        }

        if (this.gameObject.transform.position.y < triggerHeight && worldViewEnabled)
        {
            agentPositionControllers = GameObject.FindObjectsOfType<AgentPositionController>();
            endPositionController = GameObject.FindObjectOfType<EndPositionController>();
            DeactivateAll();
        }
    }

    private void ActivateAll()
    {
        //Debug.Log("Enable Worldview");
        foreach (AgentPositionController apc in agentPositionControllers) {
            apc.Activate();
        }
        endPositionController.Activate();
        worldViewEnabled = true;
    }

    private void DeactivateAll()
    {
        //Debug.Log("Disable Worldview");
        foreach (AgentPositionController apc in agentPositionControllers)
        {
            apc.Deactivate();
        }
        endPositionController.Deactivate();
        worldViewEnabled = false;
    }
}
