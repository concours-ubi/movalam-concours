# Movalam Concours Ubisoft 2021

Projet développé sous Unity 2020.2.6f1, jeu d'infiltration multijoueur.


## Technologies utilisées

- Mirror : gestion du réseau au niveau gameplay
- Steam : gestion des lobbies et parties
- Vivox : chat vocal

## Installation du jeu

Ce tutoriel est valide pour les release [0.1](https://gitlab.com/concours-ubi/movalam-concours/-/releases/0.1), [0.2](https://gitlab.com/concours-ubi/movalam-concours/-/releases/0.2) et [0.4](https://gitlab.com/concours-ubi/movalam-concours/-/releases/0.4).

### Installation de SpaceWar
Cet étape nécessite au préalable d'avoir un [compte Steam](https://store.steampowered.com/login/) ainsi que [l'application installée](https://store.steampowered.com/about/) sur votre poste de travail. Pour installer SpaceWar utilisez [ce lien](steam://install/480/). Nous devons installer ce jeu qui est utilisé par Steam comme "jeu de test". Si vous ne voulez pas déranger vos amis, pensez à vous mettre en mode hors ligne !

### Lancement du jeu
Pour lancer le jeu choisissez la version la plus récente du jeu sur la page des [release](https://gitlab.com/concours-ubi/movalam-concours/-/releases) et téléchargez l'exécutable du jeu comme montré sur l'image ci-dessous. Le dossier exécutable peut être hébergé sur Google Drive ou hébergé sur WeTransfer au format.
![Image sélection exécutable](README_ressources/Selection_executable.PNG "Téléchargement de l'exécutable")
Une fois le jeu téléchargé et extrait à l'aide de votre logiciel de compression préféré, il vous suffit d'appuyer sur le bouton play et de rejoindre un lobby ou d'en créer un après avoir sélectionné le niveau voulu.
